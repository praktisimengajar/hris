package com.mycompany.hr.timesheetservice.controller;

import com.mycompany.hr.timesheetservice.controller.administration.EmployeeController;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.auth.*;
import com.mycompany.hr.timesheetservice.service.AuthenticationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;

@CrossOrigin("*")
@RestController
@RequestMapping("auth")
@Slf4j
public class AuthenticationController {

    private final ObjectMapper mapper = new ObjectMapper();
    private final AuthenticationService service;

    public AuthenticationController(AuthenticationService service) {
        this.service = service;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<CommonResponse> doLogin(@Valid @RequestBody AuthenticationRequest login) throws JsonProcessingException {
        log.info(String.format("Entering method doLogin on class %s with payload %s", AuthenticationController.class.getName(), mapper.writeValueAsString(login)));
        CommonResponse result = service.authenticate(login);
        log.info(String.format("Leaving method doLogin on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<CommonResponse> doRegister(@Valid @RequestBody RegisterRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method doRegister on class %s with payload %s", AuthenticationController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse result = service.register(request);
        log.info(String.format("Leaving method doRegister on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    
    @PostMapping("/request-forgot-password")
    public ResponseEntity<CommonResponse> doRequestForgotPassword(@Valid @RequestBody ForgotPasswordRequest request) throws Exception {
        log.info(String.format("Entering method doRequestForgotPassword on class %s with payload %s", AuthenticationController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse result = service.requestForgotPassword(request);
        log.info(String.format("Leaving method doRequestForgotPassword on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/verify-otp")
    public ResponseEntity<CommonResponse> doValidateOtp(@Valid @RequestBody ValidateOtpRequest request) throws Exception {
        log.info(String.format("Entering method doValidateOtp on class %s with payload %s", AuthenticationController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse result = service.validateOtp(request);
        log.info(String.format("Leaving method doValidateOtp on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/change-password")
    public ResponseEntity<CommonResponse> doChangePassword(@Valid @RequestBody NewPasswordRequest request) throws JsonProcessingException, URISyntaxException {
        log.info(String.format("Entering method doChangePassword on class %s with payload %s", AuthenticationController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse result = service.changePassword(request);
        log.info(String.format("Leaving method doChangePassword on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/update-password")
    public ResponseEntity<CommonResponse> doUpdatePassword(@Valid @RequestBody UpdatePasswordRequest request) throws JsonProcessingException, URISyntaxException {
        log.info(String.format("Entering method doUpdatePassword on class %s with payload %s", AuthenticationController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse result = service.updatePassword(request);
        log.info(String.format("Leaving method doUpdatePassword on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/refresh-token")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        service.refreshToken(request, response);
    }
}
