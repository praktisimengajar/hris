package com.mycompany.hr.timesheetservice.controller;

import com.mycompany.hr.timesheetservice.model.Parameter;
import com.mycompany.hr.timesheetservice.service.ParameterService;
import com.mycompany.hr.timesheetservice.util.enumeration.ParameterType;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/parameters")
public class ParameterController {

    private final ParameterService parameterService;

    @Autowired
    public ParameterController(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    @GetMapping("/{key}")
    public ResponseEntity<Parameter> getParameter(@PathVariable String key) {
        Parameter parameter = parameterService.findByKey(key);
        if (parameter != null) {
            return ResponseEntity.ok(parameter);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<Parameter> createParameter(@Valid @RequestBody Parameter parameter) {
        return ResponseEntity.ok(parameterService.create(parameter));
    }

    @PutMapping
    public ResponseEntity<Parameter> updateParameter(@Valid @RequestBody Parameter parameter) {
        return ResponseEntity.ok(parameterService.update(parameter));
    }

    @GetMapping
    public ResponseEntity<Page<Parameter>> getAllParameters(Pageable pageable) {
        return ResponseEntity.ok(parameterService.getAllParameters(pageable));
    }

    @GetMapping(params = "/type")
    public ResponseEntity<Page<Parameter>> getParametersByType(@RequestParam ParameterType type, Pageable pageable) {
        return ResponseEntity.ok(parameterService.getParametersByType(type, pageable));
    }
}
