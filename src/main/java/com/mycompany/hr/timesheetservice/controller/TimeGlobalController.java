package com.mycompany.hr.timesheetservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@CrossOrigin(origins = "*")
@Slf4j
@RestController
public class TimeGlobalController {

    @GetMapping("/globalTime")
    public ResponseEntity<Object> getGlobalTime(@RequestParam String location) {
        try {
            String baseUrl = "http://worldtimeapi.org/api/timezone/";
            String apiUrl = baseUrl + location;

            RestTemplate restTemplate = new RestTemplate();
            Map<String, String> responseData = restTemplate.getForObject(apiUrl, Map.class);

            if (responseData != null) {
                String datetime = responseData.get("utc_datetime");
                return ResponseEntity.ok(datetime);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error fetching time data");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred: " + e.getMessage());
        }
    }
}