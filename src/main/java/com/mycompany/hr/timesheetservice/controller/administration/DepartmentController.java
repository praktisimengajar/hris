package com.mycompany.hr.timesheetservice.controller.administration;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignManager;
import com.mycompany.hr.timesheetservice.payload.request.administration.DepartmentRequest;
import com.mycompany.hr.timesheetservice.service.DepartmentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("departments")
@Slf4j
public class DepartmentController {
    private final DepartmentService service;

    private final ObjectMapper mapper = new ObjectMapper();

    public DepartmentController(DepartmentService service) {
        this.service = service;
    }

    @PostMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> addDataDepartment(@Valid @RequestBody DepartmentRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataDepartment on class %s with payload %s", DepartmentController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.addDataDepartment(request);
        log.info(String.format("Leaving method addDataDepartment on class %s", DepartmentController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> updateDataDepartment(@Valid @RequestBody DepartmentRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method updateDataDepartment on class %s with payload %s", DepartmentController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.updateDataDepartment(request);
        log.info(String.format("Leaving method updateDataDepartment on class %s", DepartmentController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getAllDataDepartments(
            @RequestParam(defaultValue = "0", name = "current-page") int page,
            @RequestParam(defaultValue = "5", name = "page-size") int size,
            @RequestParam(defaultValue = "departmentId,asc") String[] sort,
            @RequestParam(required = false) List<String> search
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getAllDepartments on class %s", DepartmentController.class.getName()));
        CommonResponse response = service.listAllDepartments(page, size, sort, search);
        log.info(String.format("Entering method getAllDepartments on class %s with response %s", DepartmentController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/assign-manager")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> assignManagerToDepartment(@Valid @RequestBody AssignManager request) throws JsonProcessingException {
        log.info(String.format("Entering method assignManagerToDepartment on class %s with payload %s", DepartmentController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignManager(request);
        log.info(String.format("Leaving method assignManagerToDepartment on class %s", DepartmentController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{department-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getDepartmentById(
            @PathVariable("department-id") Long departmentId
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getDepartmentById on class %s", DepartmentController.class.getName()));
        CommonResponse response = service.getDepartmentByDepartmentId(departmentId);
        log.info(String.format("Entering method getDepartmentById on class %s with response %s", DepartmentController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{department-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> deleteDepartment(
            @PathVariable("department-id") Long departmentId
    ) throws JsonProcessingException {
        log.info(String.format("Entering method deleteDepartment on class %s", DepartmentController.class.getName()));
        CommonResponse response = service.deleteDepartment(departmentId);
        log.info(String.format("Entering method deleteDepartment on class %s with response %s", DepartmentController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
