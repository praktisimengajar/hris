package com.mycompany.hr.timesheetservice.controller.administration;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignDepartment;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignJobRequest;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignProject;
import com.mycompany.hr.timesheetservice.payload.request.administration.EmployeeRequest;
import com.mycompany.hr.timesheetservice.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("employees")
@Slf4j
public class EmployeeController {

    private final EmployeeService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @PostMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> addDataEmployee(@Valid @RequestBody EmployeeRequest request) throws JsonProcessingException, URISyntaxException {
        log.info(String.format("Entering method addDataEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.addDataEmployee(request);
        log.info(String.format("Leaving method addDataEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> updateDataEmployee(@Valid @RequestBody EmployeeRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method updateDataEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.updateDataEmployee(request);
        log.info(String.format("Leaving method updateDataEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getAllDataEmployees(
            @RequestParam(defaultValue = "0", name = "current-page") int page,
            @RequestParam(defaultValue = "5", name = "page-size") int size,
            @RequestParam(defaultValue = "employeeId,asc") String[] sort,
            @RequestParam(required = false) String[] search
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getAllDataEmployees on class %s", EmployeeController.class.getName()));
        CommonResponse response = service.listEmployees(page, size, sort, search);
        log.info(String.format("Entering method getAllDataEmployees on class %s with response %s", EmployeeController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = {"/{employee-id}"})
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getEmployeeByEmployeeId(@PathVariable(name = "employee-id") Long employeeId) throws JsonProcessingException {
        log.info(String.format("Entering method getAllDataEmployees on class %s", EmployeeController.class.getName()));
        CommonResponse response = service.getEmployeeByEmployeeId(employeeId);
        log.info(String.format("Entering method getAllDataEmployees on class %s with response %s", EmployeeController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = {"/{employee-id}"})
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> deleteEmployee(@PathVariable(name = "employee-id") Long employeeId) {
        log.info(String.format("Entering method deleteEmployee on class %s with employee-id %d", EmployeeController.class.getName(), employeeId));
        CommonResponse response = service.deleteEmployeeById(employeeId);
        log.info(String.format("Entering method deleteEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @PostMapping("/assign-job")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> assignJobToEmployee(@Valid @RequestBody AssignJobRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method assignJobToEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignJob(request);
        log.info(String.format("Leaving method assignJobToEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/assign-department")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> assignDepartmentToEmployee(@Valid @RequestBody AssignDepartment request) throws JsonProcessingException {
        log.info(String.format("Entering method assignDepartmentToEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignDepartment(request);
        log.info(String.format("Leaving method assignDepartmentToEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/assign-project")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> assignProjectToEmployee(@Valid @RequestBody AssignProject request) throws JsonProcessingException {
        log.info(String.format("Entering method assignProjectToEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignProject(request);
        log.info(String.format("Leaving method assignProjectToEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
