package com.mycompany.hr.timesheetservice.controller.administration;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.administration.JobRequest;
import com.mycompany.hr.timesheetservice.service.JobService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("jobs")
@Slf4j
public class JobController {

    private final JobService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public JobController(JobService service) {
        this.service = service;
    }

    @PostMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> addDataJob(@Valid @RequestBody JobRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataJob on class %s with payload %s", JobController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.addDataJob(request);
        log.info(String.format("Leaving method addDataJob on class %s", JobController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> updateDataJob(@Valid @RequestBody JobRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method updateDataJob on class %s with payload %s", JobController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.updateDataJob(request);
        log.info(String.format("Leaving method updateDataJob on class %s", JobController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{job-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getJobById(
            @PathVariable("job-id") Long jobId
    ) throws JsonProcessingException {
        log.info(String.format("Leaving method getJobById on class %s", JobController.class.getName()));
        CommonResponse response = service.getJobByJobId(jobId);
        log.info(String.format("Leaving method getJobById on class %s with response %s", JobController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{job-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> deleteJob(
            @PathVariable("job-id") Long jobId
    ) throws JsonProcessingException {
        log.info(String.format("Leaving method deleteJob on class %s", JobController.class.getName()));
        CommonResponse response = service.deleteJob(jobId);
        log.info(String.format("Leaving method deleteJob on class %s with response %s", JobController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getAllDataJobs(
            @RequestParam(defaultValue = "0", name = "current-page") int page,
            @RequestParam(defaultValue = "5", name = "page-size") int size,
            @RequestParam(defaultValue = "jobId,asc") String[] sort,
            @RequestParam(required = false) List<String> search
    ) throws JsonProcessingException {
        log.info(String.format("Leaving method getAllDataJobs on class %s", JobController.class.getName()));
        CommonResponse response = service.listAllJobs(page, size, sort, search);
        log.info(String.format("Leaving method getAllDataJobs on class %s with response %s", JobController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
