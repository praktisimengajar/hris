package com.mycompany.hr.timesheetservice.controller.project;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.ClientRequest;
import com.mycompany.hr.timesheetservice.service.ClientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("clients")
@Slf4j
public class ClientController {

    private final ClientService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public ClientController(ClientService service) {
        this.service = service;
    }

    @PostMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> addDataClient(@Valid @RequestBody ClientRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataClient on class %s with payload %s", ClientController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.addDataClient(request);
        log.info(String.format("Leaving method addDataClient on class %s", ClientController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> updateDataClient(@Valid @RequestBody ClientRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method updateDataClient on class %s with payload %s", ClientController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.updateDataClient(request);
        log.info(String.format("Leaving method updateDataClient on class %s", ClientController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{client-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getClientById(
            @PathVariable("client-id") Long clientId
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getClientById on class %s", ClientController.class.getName()));
        CommonResponse response = service.getClientByClientId(clientId);
        log.info(String.format("Entering method getClientById on class %s with response %s", ClientController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{client-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> deleteClient(
            @PathVariable("client-id") Long clientId
    ) throws JsonProcessingException {
        log.info(String.format("Entering method deleteClient on class %s", ClientController.class.getName()));
        CommonResponse response = service.deleteClient(clientId);
        log.info(String.format("Entering method deleteClient on class %s with response %s", ClientController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getAllClients(
            @RequestParam(defaultValue = "0", name = "current-page") int page,
            @RequestParam(defaultValue = "5", name = "page-size") int size,
            @RequestParam(defaultValue = "clientId,asc") String[] sort,
            @RequestParam(required = false) List<String> search
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getAllClients on class %s", ClientController.class.getName()));
        CommonResponse response = service.listAllClients(page, size, sort, search);
        log.info(String.format("Entering method getAllClients on class %s with response %s", ClientController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
