package com.mycompany.hr.timesheetservice.controller.project;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.CountryRequest;
import com.mycompany.hr.timesheetservice.service.CountryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("countries")
@Slf4j
public class CountryController {

    private final CountryService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public CountryController(CountryService service) {
        this.service = service;
    }

    @PostMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> addCountry(@Valid @RequestBody CountryRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addCountry on class %s", CountryController.class.getName()));
        CommonResponse response = service.addCountry(request);
        log.info(String.format("Entering method addCountry on class %s with response %s", CountryController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getAllCountries(
            @RequestParam(defaultValue = "0", name = "current-page") int page,
            @RequestParam(defaultValue = "20", name = "page-size") int size,
            @RequestParam(defaultValue = "countryId,asc") String[] sort
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getAllCountries on class %s", CountryController.class.getName()));
        CommonResponse response = service.listAllCountries(page, size, sort);
        log.info(String.format("Entering method getAllCountries on class %s with response %s", CountryController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
