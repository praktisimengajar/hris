package com.mycompany.hr.timesheetservice.controller.project;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.EmployeeProjectRequest;
import com.mycompany.hr.timesheetservice.service.EmployeeProjectService;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("employee-project")
@Slf4j
public class EmployeeProjectController {

    private final EmployeeProjectService service;
   
    private final ObjectMapper mapper = new ObjectMapper();

    public EmployeeProjectController(EmployeeProjectService service) {
        this.service = service;
    }

    @GetMapping("/my-history")
    public ResponseEntity<CommonResponse> getEmployeeProjectHistory()  {
        log.info(String.format("Entering method getEmployeeProjectHistory on class %s", EmployeeProjectController.class.getName()));
        CommonResponse response = service.getEmployeeProjectHistory();
        log.info(String.format("Leaving method getEmployeeProjectHistory on class %s", EmployeeProjectController.class.getName()));
        return new ResponseEntity<>(new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response), HttpStatus.OK); 
    }

    @GetMapping("/my-current-project")
    public ResponseEntity<CommonResponse> getMyCurrentProject()  {
        log.info(String.format("Entering method getMyCurrentProject on class %s", EmployeeProjectController.class.getName()));
        CommonResponse response = service.getMyCurrentProject();
        log.info(String.format("Leaving method getMyCurrentProject on class %s", EmployeeProjectController.class.getName()));
        return new ResponseEntity<>(new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response), HttpStatus.OK); 
    }

    
    @PostMapping("/change-my-project")
    public ResponseEntity<CommonResponse> changeMyProject(@Valid @RequestBody EmployeeProjectRequest request)  {
        log.info(String.format("Entering method changeMyProject on class %s", EmployeeProjectRequest.class.getName()));
        CommonResponse response = service.changeMyProject(request);
        log.info(String.format("Leaving method changeMyProject on class %s", EmployeeProjectRequest.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
