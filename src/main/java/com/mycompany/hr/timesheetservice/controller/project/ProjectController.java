package com.mycompany.hr.timesheetservice.controller.project;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.AssignManagerProject;
import com.mycompany.hr.timesheetservice.payload.request.project.ProjectRequest;
import com.mycompany.hr.timesheetservice.service.ProjectService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("projects")
@Slf4j
public class ProjectController {

    private final ProjectService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public ProjectController(ProjectService service) {
        this.service = service;
    }

    @PostMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> addDataProject(@Valid @RequestBody ProjectRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataProject on class %s with payload %s", ProjectController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.addDataProject(request);
        log.info(String.format("Leaving method addDataProject on class %s", ProjectController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> updateDataProject(@Valid @RequestBody ProjectRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method updateDataProject on class %s with payload %s", ProjectController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.updateDataProject(request);
        log.info(String.format("Leaving method updateDataProject on class %s", ProjectController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getAllDataProjects(
            @RequestParam(defaultValue = "0", name = "current-page") int page,
            @RequestParam(defaultValue = "5", name = "page-size") int size,
            @RequestParam(defaultValue = "projectId,asc") String[] sort,
            @RequestParam(required = false) List<String> search
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getAllDataProjects on class %s", ProjectController.class.getName()));
        CommonResponse response = service.listAllProjects(page, size, sort, search);
        log.info(String.format("Leaving method getAllDataProjects on class %s with response %s", ProjectController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/{project-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> getProjectById(
            @PathVariable("project-id") Long projectId
    ) throws JsonProcessingException {
        log.info(String.format("Entering method getProjectById on class %s", ProjectController.class.getName()));
        CommonResponse response = service.getProjectByProjectId(projectId);
        log.info(String.format("Leaving method getProjectById on class %s with response %s", ProjectController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{project-id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> deleteProject(
            @PathVariable("project-id") Long projectId
    ) throws JsonProcessingException {
        log.info(String.format("Entering method deleteProject on class %s", ProjectController.class.getName()));
        CommonResponse response = service.deleteProject(projectId);
        log.info(String.format("Leaving method deleteProject on class %s with response %s", ProjectController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/assign-manager")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<CommonResponse> assignManagerToProject(@Valid @RequestBody AssignManagerProject request) throws JsonProcessingException {
        log.info(String.format("Entering method assignManagerToProject on class %s with payload %s", ProjectController.class.getName(), mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request)));
        CommonResponse response = service.assignManager(request);
        log.info(String.format("Leaving method assignManagerToProject on class %s", ProjectController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
