package com.mycompany.hr.timesheetservice.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class CommonException extends RuntimeException{
    @JsonProperty(value = "error_code")
    private final String errorCode;

    @JsonProperty(value = "error_message")
    private final String errorMessage;

    @JsonProperty(value = "data")
    private final transient Object data;

    @JsonProperty(value = "status_code")
    private final HttpStatus statusCode;
    
}
