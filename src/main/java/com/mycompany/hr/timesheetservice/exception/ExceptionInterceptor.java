package com.mycompany.hr.timesheetservice.exception;

import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ExceptionInterceptor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CommonException.class)
    public ResponseEntity<CommonResponse> handleApplicationException(final CommonException exception, final HttpServletRequest request) {
        
        CommonResponse response = new CommonResponse(
                exception.getErrorCode(),
                exception.getErrorMessage(),
                null
        );

        if(CommonMessage.SUCCESS_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        else if(CommonMessage.NOT_FOUND_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        else if(CommonMessage.INVALID_REQUEST_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        else if(CommonMessage.ERROR_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else if(CommonMessage.ALREADY_EXIST_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        }
        else if(CommonMessage.INTEGRITY_VIOLATION_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        else if(CommonMessage.INVALID_AUTH_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }
        else if(CommonMessage.INVALID_AUTHENTICATION_CODE.equals(exception.getErrorCode()))
        {
            return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(response, exception.getStatusCode());
    }

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }

        CommonResponse response = new CommonResponse(
                CommonMessage.INTEGRITY_VIOLATION_CODE,
                CommonMessage.INTEGRITY_VIOLATION_MESSAGE,
                errors
        );
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex, WebRequest request) {
        String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();

        CommonResponse response = new CommonResponse(
                CommonMessage.INVALID_REQUEST_CODE,
                error,
                error
        );
        return new ResponseEntity<>(
                response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    public ResponseEntity<Object> handleIllegalArgumentException(
        IllegalArgumentException ex, WebRequest request) {

        Throwable cause = ex.getCause();
        String errorMessage = "Invalid argument.";

        if (cause != null) {
                errorMessage += " Caused by: " + cause.getMessage();
        }

        if(ex.getMessage() != null){
                errorMessage += " Caused by: " + ex.getMessage();
        }
        
        String simplifiedStackTrace = "Stack Trace:" + ExceptionUtils.getStackTrace(ex);
        errorMessage += simplifiedStackTrace;
        
        CommonResponse response = new CommonResponse(
                CommonMessage.INVALID_REQUEST_CODE,
                errorMessage,
                null
        );
        return new ResponseEntity<>(
                response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex) {
        
        CommonResponse response = new CommonResponse(
                CommonMessage.INVALID_AUTHENTICATION_CODE,
                CommonMessage.INVALID_AUTHENTICATION_MESSAGE,
                null
        );
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.FORBIDDEN);    
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<Object> handleIllegalStateException(IllegalStateException ex) {
        String errorMessage = ex.getMessage();
        CommonResponse response = new CommonResponse(
                CommonMessage.INVALID_REQUEST_CODE,
                errorMessage,
                null
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, Object>> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, Object> response = new HashMap<>();

        BindingResult bindingResult = ex.getBindingResult();
        if (bindingResult != null && bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                response.put("type", "about:blank");
                response.put("title", "Bad Request");
                response.put("status", HttpStatus.BAD_REQUEST.value());
                response.put("detail", fieldError.getDefaultMessage());
                response.put("instance", "/v1/hris/auth/register");
                // or adjust the instance value accordingly based on your application logic
            }
        }

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }




}