package com.mycompany.hr.timesheetservice.model;

import com.mycompany.hr.timesheetservice.model.audit.UserDateAudit;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "configs")
public class Config extends UserDateAudit {

    @Id
    @Column(name = "config_id")
    @GeneratedValue(strategy = GenerationType.UUID)
    @UuidGenerator(style = UuidGenerator.Style.TIME)
    private UUID configId;

    @Column(name = "config_key")
    private String configKey;

    @Column(name = "config_value")
    private String configValue;
}
