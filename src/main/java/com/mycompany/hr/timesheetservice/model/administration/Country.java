package com.mycompany.hr.timesheetservice.model.administration;

import com.mycompany.hr.timesheetservice.model.audit.DateAudit;
import com.mycompany.hr.timesheetservice.model.project.Region;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * This Country model refer to ISO codes as described in the ISO 3166 international standard.
 * These codes are used throughout the IT industry by computer systems and software to ease the identification of country names.
 * */

@Getter
@Setter
@Entity
@Table(name = "countries")
public class Country extends DateAudit {

    @Id
    @SequenceGenerator(
            name = "country_id_seq",
            sequenceName = "country_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "country_id_seq")
    private Long countryId;

    @Column(name = "country_name", length = 60, nullable = false)
    private String countryName;

    @Column(name = "alpha_2_code", length = 2)
    private String alpha2Code;

    @Column(name = "alpha_3_code", length = 3)
    private String alpha3Code;

    @Column(name = "numeric_code", length = 3)
    private String numericCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id")
    @JsonIgnore
    private Region region;
}
