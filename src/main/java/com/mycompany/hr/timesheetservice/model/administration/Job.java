package com.mycompany.hr.timesheetservice.model.administration;

import com.mycompany.hr.timesheetservice.model.audit.UserDateAudit;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "jobs", uniqueConstraints = {
        @UniqueConstraint(name = "job_name_uq", columnNames = "job_name")
})
public class Job extends UserDateAudit {
    @Id
    @SequenceGenerator(
            name = "job_id_seq",
            sequenceName = "job_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "job_id_seq")
    @Column(name = "job_id", nullable = false)
    private Long jobId;

    @Column(name = "job_name", length = 50, nullable = false)
    private String jobName;

    @Column(name = "job_description", columnDefinition = "TEXT")
    private String jobDescription;

    @Column(name = "duties", columnDefinition = "TEXT")
    private String duties;

    @Column(name = "responsibilities", columnDefinition = "TEXT")
    private String responsibilities;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    @JsonIgnore
    private Department department;

    private Boolean isActive;
}
