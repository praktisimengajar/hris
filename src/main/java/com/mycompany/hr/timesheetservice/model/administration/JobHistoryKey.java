package com.mycompany.hr.timesheetservice.model.administration;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Embeddable
public class JobHistoryKey implements Serializable {

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "job_id")
    private Long jobId;

    @Column(name = "department_id")
    private Long departmentId;
}
