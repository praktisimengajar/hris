package com.mycompany.hr.timesheetservice.model.project;

import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.audit.UserDateAudit;
import com.mycompany.hr.timesheetservice.util.enumeration.TimesheetClientCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "clients", uniqueConstraints = {
        @UniqueConstraint(name = "client_name_uq", columnNames = "client_name"),
        @UniqueConstraint(name = "client_email_uq", columnNames = "email")
})
public class Client extends UserDateAudit {
    @Id
    @SequenceGenerator(
            name = "client_id_seq",
            sequenceName = "client_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id_seq")
    @Column(name = "client_id", nullable = false)
    private Long clientId;

    @Column(name = "client_name", nullable = false, length = 150)
    private String clientName;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "phone_number", length = 13)
    private String phoneNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Location location;

    @Column(columnDefinition = "TEXT")
    private String notes;

    private Boolean isActive;
}
