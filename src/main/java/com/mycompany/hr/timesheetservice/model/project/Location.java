package com.mycompany.hr.timesheetservice.model.project;

import com.mycompany.hr.timesheetservice.model.administration.Country;
import com.mycompany.hr.timesheetservice.model.audit.UserDateAudit;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "locations")
public class Location extends UserDateAudit {

    @Id
    @SequenceGenerator(
            name = "location_id_seq",
            sequenceName = "location_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_id_seq")
    private Long locationId;

    @Column(name = "street_address", length = 150, nullable = false)
    private String streetAddress;

    @Column(name = "postal_code", length = 10, nullable = false)
    private String postalCode;

    @Column(name = "city", length = 100, nullable = false)
    private String city;

    @Column(name = "province", length = 100, nullable = false)
    private String province;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    @JsonIgnore
    private Country country;

    private Boolean isActive;
}
