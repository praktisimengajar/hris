package com.mycompany.hr.timesheetservice.model.project;

import com.mycompany.hr.timesheetservice.model.audit.DateAudit;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "regions")
public class Region extends DateAudit {
    @Id
    @SequenceGenerator(
            name = "region_id_seq",
            sequenceName = "region_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "region_id_seq")
    @Column(name = "region_id", nullable = false)
    private Long regionId;

    @Column(name = "region_name", nullable = false)
    private String regionName;

    @Column(name = "sub_region_name", nullable = false)
    private String subRegionName;

    @Column(name = "continent_name", nullable = false, length = 10)
    private String continentName;

    private Boolean isActive;
}
