package com.mycompany.hr.timesheetservice.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
public record CommonResponse(
        @JsonProperty(value = "error_code")
        String errorCode,

        @JsonProperty(value = "error_message")
        String errorMessage,

        @JsonProperty(value = "data")
        Object data) implements Serializable {
}
