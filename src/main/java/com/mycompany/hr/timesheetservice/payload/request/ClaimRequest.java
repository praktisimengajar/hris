package com.mycompany.hr.timesheetservice.payload.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class ClaimRequest implements Serializable {
    public String claimName;
    public LocalDate claimDate;
    public String claimType;
    public String claimDetail;
    public MultipartFile attachment;
    public String totalAmount;
    public String spvApproval;
}
