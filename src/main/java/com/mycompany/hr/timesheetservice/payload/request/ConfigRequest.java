package com.mycompany.hr.timesheetservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ConfigRequest implements Serializable {

    @JsonProperty(value = "config_key")
    private String configKey;

    @JsonProperty(value = "config_value")
    private String configValue;
}
