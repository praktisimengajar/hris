package com.mycompany.hr.timesheetservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PagingRequest implements Serializable {
    @JsonProperty(value = "page_no")
    private Integer pageNo = 0;
    @JsonProperty(value = "page_size")
    private Integer pageSize = 10;
    @JsonProperty(value = "order_by")
    private String orderBy = "created_date";
    private Boolean descending = true;
}
