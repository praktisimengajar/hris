package com.mycompany.hr.timesheetservice.payload.request.administration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AssignJobRequest implements Serializable {

    @JsonProperty(value = "employee_id")
    private Long employeeId;

    @JsonProperty(value = "job_id")
    private Long jobId;
}
