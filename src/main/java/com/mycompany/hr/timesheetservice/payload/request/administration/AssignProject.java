package com.mycompany.hr.timesheetservice.payload.request.administration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
public class AssignProject implements Serializable {

    @JsonProperty(value = "employee_id")
    private Long employeeId;

    @JsonProperty(value = "project_id_list")
    private Set<Long> projectIdList;
}
