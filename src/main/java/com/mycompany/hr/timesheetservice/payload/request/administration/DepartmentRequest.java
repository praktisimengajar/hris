package com.mycompany.hr.timesheetservice.payload.request.administration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DepartmentRequest implements Serializable {

    @JsonProperty(value = "department_id")
    private Long departmentId;

    @JsonProperty(value = "department_name")
    private String departmentName;

    @JsonProperty(value = "department_description")
    private String description;

    @JsonProperty(value = "manager_id")
    private Long managerId;
}
