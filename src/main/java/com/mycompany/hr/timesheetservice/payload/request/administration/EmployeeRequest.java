package com.mycompany.hr.timesheetservice.payload.request.administration;

import com.mycompany.hr.timesheetservice.util.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class EmployeeRequest implements Serializable {

    @NotBlank
    @JsonProperty(value = "full_name")
    private String fullName;

    @NotBlank
    @Email
    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "gender")
    private Gender gender;

    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @JsonProperty(value = "join_date")
    private Date joinDate;

    @JsonProperty(value = "job_id")
    private Long jobId;

    @JsonProperty(value = "placement_country_id")
    private Long placementCountryId;

    @JsonProperty(value = "department_id")
    private Long departmentId;

    @JsonProperty(value = "project_id_list")
    private Set<Long> projectIdList;

    @JsonProperty(value = "client_id")
    private Long clientId;
    
    @JsonProperty(value = "is_active")
    private boolean isActive;

    @NotEmpty
    @NotBlank
    @JsonProperty(value = "role_name")
    private String roleName;

}
