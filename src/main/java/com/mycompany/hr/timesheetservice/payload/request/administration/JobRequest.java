package com.mycompany.hr.timesheetservice.payload.request.administration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class JobRequest implements Serializable {

    @JsonProperty(value = "job_id")
    private Long jobId;

    @JsonProperty(value = "job_name")
    private String jobName;

    @JsonProperty(value = "job_description")
    private String jobDescription;

    @JsonProperty(value = "duties")
    private String duties;

    @JsonProperty(value = "responsibilities")
    private String responsibilities;

    @JsonProperty(value = "department_id")
    private Long departmenId;
}
