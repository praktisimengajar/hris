package com.mycompany.hr.timesheetservice.payload.request.auth;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ChangePasswordRequest implements Serializable {

    private String email;
    private String otpCode;
    private String name;
}
