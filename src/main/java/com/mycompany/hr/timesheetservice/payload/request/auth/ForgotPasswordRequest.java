package com.mycompany.hr.timesheetservice.payload.request.auth;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ForgotPasswordRequest implements Serializable {
    private String email;
}
