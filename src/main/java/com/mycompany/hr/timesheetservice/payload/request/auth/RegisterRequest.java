package com.mycompany.hr.timesheetservice.payload.request.auth;

import com.mycompany.hr.timesheetservice.util.Password;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RegisterRequest implements Serializable {

    @NotEmpty
    @NotBlank
    @JsonProperty(value = "full_name")
    private String fullName;

    @NotEmpty
    @NotBlank
    @Email
    private String email;

    @NotEmpty
    @NotBlank
    @Password
    private String password;
}
