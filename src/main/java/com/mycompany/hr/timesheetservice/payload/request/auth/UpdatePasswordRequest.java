package com.mycompany.hr.timesheetservice.payload.request.auth;

import com.mycompany.hr.timesheetservice.util.Password;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UpdatePasswordRequest implements Serializable {

    @NotNull
    @NotBlank
    @NotEmpty
    @JsonProperty(value = "old_password")
    private String oldPassword;

    @NotNull
    @NotBlank
    @NotEmpty
    @Password
    @JsonProperty(value = "new_password")
    private String newPassword;
}
