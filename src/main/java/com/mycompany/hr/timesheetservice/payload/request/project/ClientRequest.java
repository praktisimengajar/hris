package com.mycompany.hr.timesheetservice.payload.request.project;

import com.mycompany.hr.timesheetservice.util.enumeration.TimesheetClientCode;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ClientRequest implements Serializable {

    @JsonProperty(value = "client_id")
    private Long clientId;

    @JsonProperty(value = "client_name")
    private String clientName;

    @Email
    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "notes")
    private String notes;

    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @JsonProperty(value = "location")
    private LocationRequest location;

    @JsonProperty(value = "timesheet_client_code")
    private TimesheetClientCode timesheetClientCode;
}
