package com.mycompany.hr.timesheetservice.payload.request.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CountryRequest implements Serializable{

        @JsonProperty(value = "country_name")
        public String countryName;

        @JsonProperty(value = "alpha_2_code")
        public String alpha2Code;

        @JsonProperty(value = "alpha_3_code")
        public String alpha3Code;

        @JsonProperty(value = "numeric_code")
        public String numericCode;
}
