package com.mycompany.hr.timesheetservice.payload.request.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class EmployeeProjectRequest implements Serializable {

    @JsonProperty(value = "project_id")
    private Long projectId;

    @JsonProperty(value ="start_date")
    private LocalDate startDate;

    @JsonProperty(value = "end_date")
    private LocalDate endDate;

}
