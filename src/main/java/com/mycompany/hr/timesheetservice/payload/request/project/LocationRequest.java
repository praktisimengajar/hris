package com.mycompany.hr.timesheetservice.payload.request.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LocationRequest implements Serializable {
    @JsonProperty(value = "street_address")
    private String streetAddress;

    @JsonProperty(value = "postal_code")
    private String postalCode;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "province")
    private String province;

    @JsonProperty(value = "country_id")
    private Long countryId;
}
