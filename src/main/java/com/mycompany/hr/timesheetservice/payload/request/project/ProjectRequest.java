package com.mycompany.hr.timesheetservice.payload.request.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProjectRequest implements Serializable {

    @JsonProperty(value = "project_id")
    private Long projectId;

    @JsonProperty(value = "project_name")
    private String projectName;

    @JsonProperty(value = "manager_id")
    private Long managerId;

    @JsonProperty(value = "client_id")
    private Long clientId;
}
