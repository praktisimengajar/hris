package com.mycompany.hr.timesheetservice.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ConfigResponse implements Serializable {

    @JsonProperty(value = "config_id")
    private UUID configId;

    @JsonProperty(value = "config_key")
    private String configKey;

    @JsonProperty(value = "config_value")
    private String configValue;

}
