package com.mycompany.hr.timesheetservice.payload.response.administration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record DepartmentResponse(
        @JsonProperty(value = "department_id")
        Long departmentId,

        @JsonProperty(value = "department_name")
        String departmentName,

        @JsonProperty(value = "department_description")
        String description,

        @JsonProperty(value = "manager")
        String manager,

        @JsonProperty(value = "members")
        Integer members,

        @JsonProperty(value = "list_members")
        Set<EmployeeResponse> listMembers
        ) implements Serializable {
}
