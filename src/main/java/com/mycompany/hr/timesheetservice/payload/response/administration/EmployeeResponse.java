package com.mycompany.hr.timesheetservice.payload.response.administration;

import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.payload.response.project.ClientResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.ProjectResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeResponse implements Serializable {

    @JsonProperty(value = "employee_id")
    private Long employeeId;

    @JsonProperty(value = "full_name")
    private String fullName;

    @JsonProperty(value = "gender")
    private String gender;

    @JsonProperty(value = "placement_country")
    private String placementCountry;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @JsonProperty(value = "join_date")
    private Date joinDate;

    @JsonProperty(value = "is_active")
    private boolean isActive;

    private String manager;

    @JsonProperty(value = "department_name")
    private String departmentName;

    @JsonProperty(value = "job_title")
    private String jobTitle;

    private ClientResponse client;

    @JsonProperty(value = "project_list")
    private Set<ProjectResponse> projectList;

    public static EmployeeResponse mapResponse(Employee employee) {
        EmployeeResponse response = new EmployeeResponse();
        response.setFullName(employee.getFullName());
        response.setEmail(employee.getEmail());
        response.setPhoneNumber(employee.getPhoneNumber());
        response.setJoinDate(employee.getJoinDate());
        response.setActive(employee.getIsActive());
        /*TODO fill in the project list*/
        response.setProjectList(null);
        response.setJobTitle(Objects.nonNull(employee.getJob()) ? employee.getJob().getJobName() : "");
        return response;
    }

}
