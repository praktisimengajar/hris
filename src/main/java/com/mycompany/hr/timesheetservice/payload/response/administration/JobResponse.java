package com.mycompany.hr.timesheetservice.payload.response.administration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record JobResponse(
        @JsonProperty(value = "job_id")
        Long jobId,

        @JsonProperty(value = "job_title")
        String jobName,

        @JsonProperty(value = "job_description")
        String jobDescription,

        String duties,

        String responsibilities,

        @JsonProperty(value = "department_name")
        String departmentName,

        Integer members,

        @JsonProperty(value = "list_members")
        Set<EmployeeResponse> listMembers
        ) implements Serializable {
}
