package com.mycompany.hr.timesheetservice.payload.response.auth;

import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.ProjectResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record AuthenticationResponse (
        @JsonProperty("access_token")
        String accessToken,
        @JsonProperty("refresh_token")
        String refreshToken,
        @JsonProperty(value = "employee")
        EmployeeResponse employee,
        @JsonProperty(value = "project_list")
        Set<ProjectResponse> projectList,
        @JsonProperty(value = "role_type")
        String roleType,
        @JsonProperty(value = "user")
        UserResponse user
        ) implements Serializable {
}
