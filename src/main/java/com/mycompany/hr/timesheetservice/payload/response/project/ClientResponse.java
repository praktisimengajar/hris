package com.mycompany.hr.timesheetservice.payload.response.project;

import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record ClientResponse (
        @JsonProperty(value = "client_id")
        Long clientId,
        @JsonProperty(value = "client_name")
        String clientName,
        String email,
        String notes,

        String phoneNumber,
        LocationResponse location,

        @JsonProperty(value = "list_members")
        Set<EmployeeResponse> listMembers
        ) implements Serializable {
}
