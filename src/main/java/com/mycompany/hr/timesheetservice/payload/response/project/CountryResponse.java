package com.mycompany.hr.timesheetservice.payload.response.project;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record CountryResponse(

        @JsonProperty(value = "country_id")
        Long countryId,

        @JsonProperty(value = "country_name")
        String countryName,

        @JsonProperty(value = "alpha_2_code")
        String alpha2Code,

        @JsonProperty(value = "alpha_3_code")
        String alpha3Code,

        @JsonProperty(value = "numeric_code")
        String numericCode
) implements Serializable {
}
