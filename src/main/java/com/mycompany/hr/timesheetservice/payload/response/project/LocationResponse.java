package com.mycompany.hr.timesheetservice.payload.response.project;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record LocationResponse(

        @JsonProperty(value = "street_address")
        String streetAddress,

        @JsonProperty(value = "postal_code")
        String postalCode,
        String city,
        String province,

        String country
) implements Serializable {
}
