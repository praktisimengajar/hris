package com.mycompany.hr.timesheetservice.payload.response.project;

import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record ProjectResponse(
        @JsonProperty(value = "project_id")
        Long projectId,

        @JsonProperty(value = "project_name")
        String projectName,

        @JsonProperty(value = "project_notes")
        String projectNotes,

        @JsonProperty(value = "manager")
        String manager,
        
        @JsonProperty(value = "client")
        ClientResponse client,

        Integer members,

        @JsonProperty(value = "list_members")
        Set<EmployeeResponse> listMembers
        ) implements Serializable {
}
