package com.mycompany.hr.timesheetservice.repository;

import com.mycompany.hr.timesheetservice.model.Config;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@Hidden
public interface ConfigRepository extends JpaRepository<Config, UUID> {
    boolean existsByConfigKey(String configKey);
    List<Config> findAllByConfigKey(String configKey);
}
