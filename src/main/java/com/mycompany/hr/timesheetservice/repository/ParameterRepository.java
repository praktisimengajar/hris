package com.mycompany.hr.timesheetservice.repository;

import com.mycompany.hr.timesheetservice.model.Parameter;
import com.mycompany.hr.timesheetservice.util.enumeration.ParameterType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long> {
    Parameter findByKey(String key);
    List<Parameter> findListByType(ParameterType type);
    Page<Parameter> findPageByType(ParameterType type, Pageable pageable);
    Page<Parameter> findAll(Pageable pageable);
}

