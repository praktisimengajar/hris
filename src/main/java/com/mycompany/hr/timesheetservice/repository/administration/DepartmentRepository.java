package com.mycompany.hr.timesheetservice.repository.administration;

import com.mycompany.hr.timesheetservice.model.administration.Department;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Hidden
public interface DepartmentRepository extends JpaRepository<Department, Long>, JpaSpecificationExecutor<Department> {
    boolean existsByDepartmentName(String departmentName);
    Optional<Department> findByDepartmentIdAndIsActive(Long departmentId, boolean isActive);
}
