package com.mycompany.hr.timesheetservice.repository.administration;

import com.mycompany.hr.timesheetservice.model.administration.Department;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.administration.Job;
import com.mycompany.hr.timesheetservice.model.project.Client;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Hidden
public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {
    Page<Employee> findAllByIsActive(Boolean isActive, Pageable pageable);
    boolean existsByEmail(String email);
    boolean existsByPhoneNumber(String phoneNumber);
    Optional<Employee> findByEmailAndIsActive(String email, boolean isActive);
    Optional<Employee> findByEmployeeIdAndIsActive(Long employeeId, boolean isActive);
    Optional<Employee> findByEmail(String email);

    long countByJob_JobId(Long jobId);

    long countByDepartment_DepartmentId(Long departmentId);

    List<Employee> findAllByIsActiveAndDepartment(boolean isActive, Department department);
    List<Employee> findAllByIsActiveAndJob(boolean isActive, Job job);
}
