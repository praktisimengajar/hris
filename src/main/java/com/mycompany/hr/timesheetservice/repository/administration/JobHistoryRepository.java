package com.mycompany.hr.timesheetservice.repository.administration;

import com.mycompany.hr.timesheetservice.model.administration.Job;
import com.mycompany.hr.timesheetservice.model.administration.JobHistoryKey;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface JobHistoryRepository extends JpaRepository<Job, JobHistoryKey> {
}
