package com.mycompany.hr.timesheetservice.repository.project;

import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProject;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProjectKey;
import com.mycompany.hr.timesheetservice.model.project.Project;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Hidden
public interface EmployeeProjectRepository extends JpaRepository<EmployeeProject, EmployeeProjectKey> {
    List<EmployeeProject> findByEmployeeAndIsActive(Employee employee, boolean isActive);
    List<EmployeeProject> findByEmployee(Employee employee);
    long countByProject_ProjectIdAndIsActive(Long projectId, boolean isActive);
    List<EmployeeProject> findAllByProjectAndProject_IsActive(Project project, boolean isActive);
}