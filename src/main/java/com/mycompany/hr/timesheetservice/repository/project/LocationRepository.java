package com.mycompany.hr.timesheetservice.repository.project;

import com.mycompany.hr.timesheetservice.model.project.Location;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Hidden
public interface LocationRepository extends JpaRepository<Location, Long> {
    Optional<Location> findByStreetAddress(String streetAddress);
}
