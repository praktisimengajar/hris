package com.mycompany.hr.timesheetservice.repository.project;

import com.mycompany.hr.timesheetservice.model.project.Project;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Hidden
public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project> {
    boolean existsByProjectName(String projectName);
    Optional<Project> findTopByStatusOrderByProjectIdDesc(Boolean isActive);
}