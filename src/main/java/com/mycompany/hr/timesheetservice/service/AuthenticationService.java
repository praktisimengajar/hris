package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.auth.Token;
import com.mycompany.hr.timesheetservice.model.auth.User;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.auth.*;
import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.mycompany.hr.timesheetservice.payload.response.auth.AuthenticationResponse;
import com.mycompany.hr.timesheetservice.payload.response.auth.UserResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.ProjectResponse;
import com.mycompany.hr.timesheetservice.repository.auth.TokenRepository;
import com.mycompany.hr.timesheetservice.repository.auth.UserRepository;
import com.mycompany.hr.timesheetservice.repository.project.EmployeeProjectRepository;
import com.mycompany.hr.timesheetservice.security.JwtService;
import com.mycompany.hr.timesheetservice.util.*;
import com.mycompany.hr.timesheetservice.util.enumeration.RoleType;
import com.mycompany.hr.timesheetservice.util.enumeration.TokenType;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;


@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {
    private String notificationURL;
    private final UserRepository repository;
    private final TokenRepository tokenRepository;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final EmployeeService employeeService;
    private final EmployeeProjectRepository employeeProjectRepository;
    private final OtpGenerator otpGenerator;
    private final RestTemplate restTemplate;
    private final PasswordEncoder passwordEncoder;
    

    public CommonResponse authenticate(AuthenticationRequest request) {
        User user = emailValidator(request.getEmail());

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );

        if (!authentication.isAuthenticated()) {
            throw new CommonException(
                    CommonMessage.INVALID_AUTHENTICATION_CODE,
                    CommonMessage.INVALID_AUTHENTICATION_MESSAGE,
                    null,
                    null
            );
        }

        String jwtToken = jwtService.generateToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);

        AuthenticationResponse response;

        EmployeeResponse employee = new EmployeeResponse();
        Set<ProjectResponse> projectList = new HashSet<>();

        if(!request.isNonEmployee()){
        Employee dataEmployee = employeeService.getEmployeeByEmail(request.getEmail());
        employee.setEmployeeId(dataEmployee.getEmployeeId());
        employee.setFullName(dataEmployee.getFullName());
        employee.setEmail(dataEmployee.getEmail());
        employee.setPhoneNumber(dataEmployee.getPhoneNumber());
        employee.setJoinDate(dataEmployee.getJoinDate());
        employee.setJobTitle(dataEmployee.getJob().getJobName());

        
        employeeProjectRepository.findByEmployeeAndIsActive(dataEmployee, true).forEach(employeeProject -> {
            ProjectResponse project = new ProjectResponse(
                    employeeProject.getProject().getProjectId(),
                    employeeProject.getProject().getProjectName(),
                    employeeProject.getProject().getNotes(),
                    Objects.isNull(employeeProject.getProject().getManager()) ? null: employeeProject.getProject().getManager().getFullName(), 
                    null, null, null
            );
            projectList.add(project);
        });
        }

        response = new AuthenticationResponse(jwtToken, refreshToken, employee, projectList, user.getRole().toString().toLowerCase(), new UserResponse(user.getUserId(), user.getFullName(), user.getEmail()));

        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE,
                response);
    }

    public CommonResponse requestForgotPassword(ForgotPasswordRequest request) throws Exception {
        User user = emailValidator(request.getEmail());
        String otp = otpGenerator.generateOTP(user.getEmail());

        Employee employee = employeeService.getEmployeeByEmail(request.getEmail());

        // TODO Change to message broker
        URI uri = new URI(notificationURL+ "/forgot-password");
        ChangePasswordRequest changePassword = new ChangePasswordRequest();
        changePassword.setEmail(employee.getEmail());
        changePassword.setOtpCode(otp);
        changePassword.setName(employee.getFullName());

    
        CompletableFuture.runAsync(() -> {
            restTemplate.postForEntity(uri, changePassword, String.class);
        });

        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse validateOtp(ValidateOtpRequest request) throws Exception {
        User user = emailValidator(request.getEmail());

        otpGenerator.verifyOTP(user.getEmail(),request.getOtpCode());
        
        return new CommonResponse(
                CommonMessage.SUCCESS_CODE,
                CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse changePassword(NewPasswordRequest request) {
        User user = emailValidator(request.getEmail());

        if(!otpGenerator.isVerified(user.getEmail())) {
            throw new CommonException(CommonMessage.INVALID_REQUEST_CODE, "Please verify your OTP first.", null, null);
        }

        CommonResponse response;

        if (CommonValidator.passwordIsValid(request.getPassword())) {
            user.setPassword(passwordEncoder.encode(request.getPassword()));
            repository.saveAndFlush(user);

            AuthenticationRequest requests = new AuthenticationRequest();
            requests.setEmail(request.getEmail());
            requests.setPassword(request.getPassword());
            response = this.authenticate(requests);
        } else {
            throw new CommonException(
                    CommonMessage.INVALID_AUTHENTICATION_CODE,
                    String.format("The password your enter is %s", CommonMessage.INVALID_MESSAGE),
                    null,
                    null);
        }

        return response;
    }

    public CommonResponse updatePassword(UpdatePasswordRequest request) {
        User user = emailValidator(CommonUtil.currentUser());
        CommonResponse response; 
        
        if(passwordEncoder.matches(request.getOldPassword(), user.getPassword())){
            user.setPassword(passwordEncoder.encode(request.getNewPassword()));
            repository.saveAndFlush(user);

            AuthenticationRequest requests = new AuthenticationRequest();
            requests.setEmail(CommonUtil.currentUser());
            requests.setPassword(request.getNewPassword());
            response = this.authenticate(requests);
        } else {
            throw new CommonException(
                    CommonMessage.INVALID_AUTHENTICATION_CODE,
                    String.format("The password your enter is %s", CommonMessage.INVALID_MESSAGE),
                    null,
                    null);
        }

        return response;
    }

    private User emailValidator(String email) {        return repository.findByEmail(email)
                .orElseThrow(() -> new CommonException(
                        CommonMessage.INVALID_AUTH_CODE,
                        String.format("Email %s ".concat(CommonMessage.NOT_FOUND_MESSAGE), email),
                        null,
                        HttpStatus.UNAUTHORIZED
                ));
    }

    private void saveUserToken(User user, String jwtToken) {
        Token token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getUserId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;
        if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
            return;
        }
        refreshToken = authHeader.substring(7);
        userEmail = jwtService.extractUsername(refreshToken);
        if (userEmail != null) {
            User user = this.repository.findByEmail(userEmail)
                    .orElseThrow();
            if (jwtService.isTokenValid(refreshToken, user)) {
                String accessToken = jwtService.generateToken(user);
                revokeAllUserTokens(user);
                saveUserToken(user, accessToken);

                Employee dataEmployee = employeeService.getEmployeeByEmail(userEmail);
                EmployeeResponse employee = new EmployeeResponse();
                employee.setFullName(dataEmployee.getFullName());
                employee.setEmail(dataEmployee.getFullName());
                employee.setPhoneNumber(dataEmployee.getPhoneNumber());
                employee.setJoinDate(dataEmployee.getJoinDate());
                employee.setJobTitle(dataEmployee.getJob().getJobName());

                Set<ProjectResponse> projectList = new HashSet<>();
                employeeProjectRepository.findByEmployeeAndIsActive(dataEmployee, true).forEach(employeeProject -> {
                    ProjectResponse responses = new ProjectResponse(
                            employeeProject.getProject().getProjectId(),
                            employeeProject.getProject().getProjectName(),
                            employeeProject.getProject().getNotes(),
                            Objects.isNull(employeeProject.getProject().getManager()) ? null: employeeProject.getProject().getManager().getFullName(), null, null, null
                    );
                    projectList.add(responses);
                });

                new ObjectMapper().writeValue(response.getOutputStream(),
                        new AuthenticationResponse(accessToken, refreshToken, employee, projectList, user.getRole().toString().toLowerCase(), null));
            }
        }
    }

    public CommonResponse register(RegisterRequest request) {
        if (repository.existsByEmail(request.getEmail())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Email %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getEmail()),
                    null,
                    HttpStatus.OK);
        }

        User user = User.builder()
                .fullName(request.getFullName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(RoleType.ROLE_ADMIN)
                .build();
        repository.save(user);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}
