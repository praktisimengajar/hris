package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.project.Client;
import com.mycompany.hr.timesheetservice.model.project.Location;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.ClientRequest;
import com.mycompany.hr.timesheetservice.payload.response.project.ClientResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.LocationResponse;
import com.mycompany.hr.timesheetservice.repository.project.ClientRepository;
import com.mycompany.hr.timesheetservice.repository.project.LocationRepository;
import com.mycompany.hr.timesheetservice.repository.project.ProjectRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.mycompany.hr.timesheetservice.util.specification.ClientSpecification.*;

@Service
@Slf4j
public class ClientService {

    private final ClientRepository repository;
    private final CountryService countryService;
    private final LocationRepository locationRepository;

    public ClientService(ClientRepository repository, CountryService countryService, LocationRepository locationRepository ) {
        this.repository = repository;
        this.countryService = countryService;
        this.locationRepository = locationRepository;
    }

    public CommonResponse addDataClient(ClientRequest request) {
        if (repository.existsByClientName(request.getClientName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Client Name %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getClientName()),
                    null,
                    HttpStatus.OK);
        }

        Client client = new Client();
        return setClientValueFromRequest(request, client);
    }

    public CommonResponse listAllClients(int page, int size, String[] sort, List<String> search) {
        Page<Client> clientList;
        Specification<Client> specification = isActive();
        if (Objects.isNull(search)) {
            clientList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else if (search.isEmpty()) {
            clientList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else {
            String columnName = search.get(0);
            String searchValue = search.get(1);

            if (search.get(0).equalsIgnoreCase("countryName")) {
                specification = hasCountryName(searchValue).and(isActive());
            } else {
                specification = hasName(columnName, searchValue).and(isActive());
            }

            clientList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        }

        List<ClientResponse> clients = new ArrayList<>();

        clientList.forEach(client -> {
            LocationResponse location = new LocationResponse(
                    client.getLocation().getStreetAddress(),
                    client.getLocation().getPostalCode(),
                    client.getLocation().getCity(),
                    client.getLocation().getProvince(),
                    client.getLocation().getCountry().getCountryName()
            );

            clients.add(new ClientResponse(
                    client.getClientId(), client.getClientName(),
                    client.getEmail(), client.getNotes(),
                    client.getPhoneNumber(), location, null
            ));
        });

        Map<String, Object> response = new HashMap<>();
        response.put("clients", clients);
        response.put("current_page", clientList.getNumber());
        response.put("total_items", clientList.getTotalElements());
        response.put("total_pages", clientList.getTotalPages());
        response.put("page_size", clientList.getSize());

        if (!clients.isEmpty()) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
        } else {
            return new CommonResponse(CommonMessage.NOT_FOUND_CODE, CommonMessage.NOT_FOUND_MESSAGE, CommonMessage.NOT_FOUND_MESSAGE);
        }
    }

    public CommonResponse getClientByClientId(Long clientId) {
        Client client = getClientById(clientId);
        LocationResponse location = new LocationResponse(
                client.getLocation().getStreetAddress(), client.getLocation().getPostalCode(),
                client.getLocation().getCity(), client.getLocation().getProvince(), client.getLocation().getCountry().getCountryName()
        );


        ClientResponse response = new ClientResponse(
                client.getClientId(), client.getClientName(), client.getEmail(), client.getNotes(),
                client.getPhoneNumber(), location, null
        );
        
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
    }

    public Client getClientById(Long clientId) {
        return repository.findById(clientId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for client your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }

    public CommonResponse updateDataClient(ClientRequest request) {
        Client client = getClientById(request.getClientId());
        return setClientValueFromRequest(request, client);
    }

    @NonNull
    private CommonResponse setClientValueFromRequest(ClientRequest request, Client client) {
        client.setClientName(request.getClientName());
        client.setEmail(request.getEmail());
        client.setNotes(request.getNotes());
        client.setPhoneNumber(request.getPhoneNumber());
        client.setIsActive(true);

        Location location = new Location();
        if (!Objects.isNull(request.getLocation())) {
            location.setStreetAddress(request.getLocation().getStreetAddress());
            location.setPostalCode(request.getLocation().getPostalCode());
            location.setCity(request.getLocation().getCity());
            location.setProvince(request.getLocation().getProvince());
            location.setCountry(countryService.getCountryById(request.getLocation().getCountryId()));
            locationRepository.saveAndFlush(location);
        } else {
            client.setLocation(null);
        }

        client.setLocation(location);
        repository.save(client);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse deleteClient(Long clientId) {
        Client client = getClientById(clientId);
        client.setIsActive(true);
        repository.save(client);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}
