package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.Config;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.ConfigRequest;
import com.mycompany.hr.timesheetservice.payload.response.ConfigResponse;
import com.mycompany.hr.timesheetservice.repository.ConfigRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ConfigService {

    private final ConfigRepository repository;

    public ConfigService(ConfigRepository repository) {
        this.repository = repository;
    }

    public CommonResponse addConfig(ConfigRequest request) {
        if (repository.existsByConfigKey(request.getConfigKey())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Config Key %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getConfigKey()),
                    null,
                    HttpStatus.OK);
        }

        Config config = new Config();
        config.setConfigKey(request.getConfigKey());
        config.setConfigValue(request.getConfigValue());
        repository.save(config);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public List<ConfigResponse> getAllConfigByKey(String configKey) {
        List<Config> configList = repository.findAllByConfigKey(configKey);

        if (configKey.isEmpty()) {
            throw new CommonException(
                    CommonMessage.NOT_FOUND_MESSAGE,
                    String.format("Config Value %s ".concat(CommonMessage.NOT_FOUND_MESSAGE), configKey),
                    null,
                    HttpStatus.OK);
        }

        List<ConfigResponse> responses = new ArrayList<>();
        configList.forEach(config -> {
            ConfigResponse response = new ConfigResponse();
            response.setConfigId(config.getConfigId());
            response.setConfigKey(config.getConfigKey());
            response.setConfigValue(config.getConfigValue());
            responses.add(response);
        });

        return responses;
    }

    public CommonResponse getAllData(int page, int size, String[] sort, List<String> search) {
        Page<Config> configList = repository.findAll(CommonUtil.paging(page, size, sort));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}