package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Country;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.CountryRequest;
import com.mycompany.hr.timesheetservice.payload.response.project.CountryResponse;
import com.mycompany.hr.timesheetservice.repository.project.CountryRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mycompany.hr.timesheetservice.util.CountrySpecification.hasRegionName;

@Service
@Slf4j
public class CountryService {

    private final CountryRepository repository;

    public CountryService(CountryRepository repository) {
        this.repository = repository;
    }

    public CommonResponse addCountry(CountryRequest countryRequest) {
        Country country = new Country();
        country.setCountryName(countryRequest.getCountryName());
        country.setNumericCode(countryRequest.getNumericCode());
        country.setAlpha2Code(countryRequest.getAlpha2Code());
        country.setAlpha3Code(countryRequest.getAlpha3Code());
        repository.save(country);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public Country getCountryById(Long countryId) {
        return repository.findById(countryId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for country your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                null
                ));
    }

    public CommonResponse listAllCountries(int page, int size, String[] sort) {
        Specification<Country> specification = hasRegionName("");
        Page<Country> countryPage = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        if(countryPage.getContent().isEmpty()){
            return new CommonResponse(
                CommonMessage.NOT_FOUND_CODE,
                CommonMessage.NOT_FOUND_MESSAGE,
                CommonMessage.NOT_FOUND_MESSAGE
             );
        }

        List<CountryResponse> countries = new ArrayList<>();
        countryPage.getContent().forEach(country -> countries.add(new CountryResponse(
                country.getCountryId(),
                country.getCountryName(),
                country.getAlpha2Code(),
                country.getAlpha3Code(),
                country.getNumericCode())));

        Map<String, Object> response = new HashMap<>();
        response.put("countries", countries);
        response.put("current_page", countryPage.getNumber());
        response.put("total_items", countryPage.getTotalElements());
        response.put("total_pages", countryPage.getTotalPages());

        return new CommonResponse(
                    CommonMessage.SUCCESS_CODE,
                    CommonMessage.SUCCESS_MESSAGE,
                    response
        );
    }
}
