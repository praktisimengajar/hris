package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Department;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignManager;
import com.mycompany.hr.timesheetservice.payload.request.administration.DepartmentRequest;
import com.mycompany.hr.timesheetservice.payload.response.administration.DepartmentResponse;
import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.mycompany.hr.timesheetservice.repository.administration.DepartmentRepository;
import com.mycompany.hr.timesheetservice.repository.administration.EmployeeRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.mycompany.hr.timesheetservice.util.specification.DepartmentSpecification.hasName;
import static com.mycompany.hr.timesheetservice.util.specification.DepartmentSpecification.isActive;

@Service
@Slf4j
public class DepartmentService {
    private final DepartmentRepository repository;
    private final EmployeeRepository employeeRepository;

    public DepartmentService(DepartmentRepository repository, EmployeeRepository employeeRepository) {
        this.repository = repository;
        this.employeeRepository = employeeRepository;
    }

    public CommonResponse addDataDepartment(DepartmentRequest request) {
        if (repository.existsByDepartmentName(request.getDepartmentName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Department %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getDepartmentName()),
                    null,
                    HttpStatus.OK);
        }

        Department department = new Department();
        return setDepartmentValueFromResponse(request, department);
    }

    public CommonResponse listAllDepartments(int page, int size, String[] sort, List<String> search) {
        Page<Department> departmentList;
        Specification<Department> specification = isActive();
        if (Objects.isNull(search)) {
            departmentList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else if (search.isEmpty()) {
            departmentList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else {
            String columnName = search.get(0);
            String searchValue = search.get(1);

            specification = hasName(columnName, searchValue).and(isActive());
            departmentList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        }

        List<DepartmentResponse> departments = new ArrayList<>();
        departmentList.forEach(data -> departments.add(new DepartmentResponse(
                data.getDepartmentId(), data.getDepartmentName(), data.getDescription(),
                !Objects.isNull(data.getManager()) ? data.getManager().getFullName() : null,
                Math.toIntExact(employeeRepository.countByDepartment_DepartmentId(data.getDepartmentId())),
                null))
        );

        Map<String, Object> response = new HashMap<>();
        response.put("departments", departments);
        response.put("current_page", departmentList.getNumber());
        response.put("total_items", departmentList.getTotalElements());
        response.put("total_pages", departmentList.getTotalPages());
        response.put("page_size", departmentList.getSize());

        if (!departments.isEmpty()) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
        } else {
            return new CommonResponse(CommonMessage.NOT_FOUND_CODE, CommonMessage.NOT_FOUND_MESSAGE, CommonMessage.NOT_FOUND_MESSAGE);
        }
    }

    public CommonResponse assignManager(AssignManager request) {
        Optional<Department> dataDepartment = repository.findById(request.getDepartmentId());
        AtomicReference<String> code = new AtomicReference<>();
        AtomicReference<String> message = new AtomicReference<>();
        AtomicReference<String> response = new AtomicReference<>();
        dataDepartment.ifPresentOrElse(data -> {
            Optional<Employee> dataEmployee = employeeRepository.findById(request.getManagerId());
            dataEmployee.ifPresentOrElse(manager -> {
                data.setManager(manager);
                repository.save(data);
                code.set(CommonMessage.SUCCESS_CODE);
                message.set(CommonMessage.SUCCESS_MESSAGE);
                response.set(CommonMessage.SUCCESS_MESSAGE);
            }, () -> {
                code.set(CommonMessage.NOT_FOUND_CODE);
                message.set(CommonMessage.NOT_FOUND_MESSAGE);
                response.set(String.format("Employee %s", CommonMessage.NOT_FOUND_MESSAGE));
            });
        }, () -> {
            code.set(CommonMessage.NOT_FOUND_CODE);
            message.set(CommonMessage.NOT_FOUND_MESSAGE);
            response.set(String.format("Department %s", CommonMessage.NOT_FOUND_MESSAGE));
        });
        return new CommonResponse(code.get(), message.get(), response.get());
    }

    public Department getDepartmentById(Long departmentId) {
        return repository.findByDepartmentIdAndIsActive(departmentId, true).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for department your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }


    public CommonResponse getDepartmentByDepartmentId(Long departmentId) {
        Department department = getDepartmentById(departmentId);

        Set<EmployeeResponse> listMembers = new HashSet<>();
        List<Employee> employees = employeeRepository.findAllByIsActiveAndDepartment(true, department);
        employees.forEach(employee -> {
            EmployeeResponse response = new EmployeeResponse();
            response.setEmployeeId(employee.getEmployeeId());
            response.setFullName(employee.getFullName());
            response.setGender(CommonUtil.capitalize(employee.getGender().name()));
            response.setPlacementCountry(employee.getCountry().getCountryName());
            response.setJobTitle(employee.getJob().getJobName());
            listMembers.add(response);
        });

        DepartmentResponse response = new DepartmentResponse(
                department.getDepartmentId(), department.getDepartmentName(), department.getDescription(),
                !Objects.isNull(department.getManager()) ? department.getManager().getFullName() : null,
                null,
                listMembers
        );

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
    }

    public CommonResponse updateDataDepartment(DepartmentRequest request) {
        Department department = this.getDepartmentById(request.getDepartmentId());
        return setDepartmentValueFromResponse(request, department);
    }

    @NonNull
    private CommonResponse setDepartmentValueFromResponse(DepartmentRequest request, Department department) {
        department.setDepartmentName(request.getDepartmentName());
        department.setDescription(request.getDescription());
        department.setIsActive(true);
        repository.save(department);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse deleteDepartment(Long departmentId) {
        Department department = this.getDepartmentById(departmentId);
        department.setIsActive(false);
        repository.save(department);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}
