package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProject;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProjectKey;
import com.mycompany.hr.timesheetservice.model.project.Project;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.EmployeeProjectRequest;
import com.mycompany.hr.timesheetservice.payload.response.project.ClientResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.EmployeeProjectResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.ProjectResponse;
import com.mycompany.hr.timesheetservice.repository.project.EmployeeProjectRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class EmployeeProjectService {

    private final EmployeeProjectRepository repository;
    private final EmployeeService employeeService;
    private final ProjectService projectService;

    public EmployeeProjectService(EmployeeProjectRepository repository, EmployeeService employeeService, ProjectService projectService) {
        this.repository = repository;
        this.employeeService = employeeService;
        this.projectService = projectService;
    }

    public CommonResponse getEmployeeProjectHistory() {
        Employee employeeData = employeeService.getEmployeeByEmail(CommonUtil.currentUser());
        List <EmployeeProject> dataEmployee = repository.findByEmployee(employeeData);
       if(dataEmployee.isEmpty()){
        throw new CommonException(
            CommonMessage.NOT_FOUND_CODE, 
            CommonMessage.NOT_FOUND_MESSAGE, 
            "Project history is not found",
            null
            );
       }

       List <EmployeeProjectResponse> employeeProjectResponse = new ArrayList<>();
       dataEmployee.forEach(
        data -> {
            EmployeeProjectResponse employeeProjectResponseTemp = new EmployeeProjectResponse();
            employeeProjectResponseTemp.setProject(
                new ProjectResponse(data.getProject().getProjectId(), data.getProject().getProjectName(),
                        data.getProject().getNotes(),null,
                        new ClientResponse(data.getProject().getClient().getClientId(), data.getProject().getClient().getClientName(), null, null, null, null, null), null, null)
                );
            employeeProjectResponseTemp.setStartDate(data.getStartDate());
            employeeProjectResponseTemp.setEndDate(data.getEndDate());
            employeeProjectResponseTemp.setIsActive(data.getIsActive());
            employeeProjectResponse.add(employeeProjectResponseTemp);
        }
       );
       
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, employeeProjectResponse);
    }

    public CommonResponse getMyCurrentProject() {
        Employee employeeData = employeeService.getEmployeeByEmail(CommonUtil.currentUser());
        List <EmployeeProject> dataEmployee = repository.findByEmployeeAndIsActive(employeeData,true);
        if(dataEmployee.isEmpty()){
            throw new CommonException(
                CommonMessage.NOT_FOUND_CODE, 
                CommonMessage.NOT_FOUND_MESSAGE, 
                "Current project is not found",
                null
                );
           }

        EmployeeProject result = dataEmployee.get(0);
        EmployeeProjectResponse employeeProjectResponseTemp = new EmployeeProjectResponse();
        employeeProjectResponseTemp.setProject(
            new ProjectResponse(result.getProject().getProjectId(), result.getProject().getProjectName(),
                    result.getProject().getNotes(), null,
                    new ClientResponse(result.getProject().getClient().getClientId(), result.getProject().getClient().getClientName(), null, null, null, null, null), null, null)
        );
            employeeProjectResponseTemp.setStartDate(result.getStartDate());
            employeeProjectResponseTemp.setEndDate(result.getEndDate());
            employeeProjectResponseTemp.setIsActive(result.getIsActive());
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, employeeProjectResponseTemp);
    }

    public CommonResponse changeMyProject(EmployeeProjectRequest employeeProjectRequest) {
        Employee employeeData = employeeService.getEmployeeByEmail(CommonUtil.currentUser());
        List <EmployeeProject> dataEmployee = repository.findByEmployeeAndIsActive(employeeData,true);

        dataEmployee.forEach( data -> {
            data.setEndDate(LocalDate.now());
            data.setIsActive(false);
            repository.save(data);
        });

        Project project = projectService.getProjectById(employeeProjectRequest.getProjectId());
        EmployeeProject employeeProject = new EmployeeProject();
        EmployeeProjectKey employeeProjectKey = new EmployeeProjectKey();
        employeeProjectKey.setEmployeeId(employeeData.getEmployeeId());
        employeeProjectKey.setProjectId(project.getProjectId());
        employeeProject.setEmployeeProjectId(employeeProjectKey);
        employeeProject.setEmployee(employeeData);
        employeeProject.setProject(project);
        employeeProject.setStartDate(employeeProjectRequest.getStartDate() == null ? LocalDate.now(): employeeProjectRequest.getStartDate());
        employeeProject.setEndDate(employeeProjectRequest.getEndDate() == null ? LocalDate.now().plusMonths(6): employeeProjectRequest.getEndDate());
        employeeProject.setIsActive(true);

        EmployeeProject result = repository.save(employeeProject);
        
        EmployeeProjectResponse employeeProjectResponseTemp = new EmployeeProjectResponse();
        employeeProjectResponseTemp.setProject(
        new ProjectResponse(result.getProject().getProjectId(), result.getProject().getProjectName(), result.getProject().getNotes(), null,
        new ClientResponse(result.getProject().getClient().getClientId(), result.getProject().getClient().getClientName(), null, null, null, null, null), null, null)
        );
        employeeProjectResponseTemp.setStartDate(result.getStartDate());
        employeeProjectResponseTemp.setEndDate(result.getEndDate());
        employeeProjectResponseTemp.setIsActive(result.getIsActive());

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, employeeProjectResponseTemp);
    }
}
