package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.auth.User;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProject;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProjectKey;
import com.mycompany.hr.timesheetservice.model.project.Project;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignDepartment;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignJobRequest;
import com.mycompany.hr.timesheetservice.payload.request.administration.AssignProject;
import com.mycompany.hr.timesheetservice.payload.request.administration.EmployeeRequest;
import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.mycompany.hr.timesheetservice.repository.administration.EmployeeRepository;
import com.mycompany.hr.timesheetservice.repository.auth.UserRepository;
import com.mycompany.hr.timesheetservice.repository.project.EmployeeProjectRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.mycompany.hr.timesheetservice.util.specification.EmployeeSpecification.*;

@Service
@Slf4j
public class EmployeeService {
    private final EmployeeRepository repository;
    private final JobService jobService;
    private final CountryService countryService;
    private final DepartmentService departmentService;
    private final ProjectService projectService;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final EmployeeProjectRepository employeeProjectRepository;
    private final ParameterService parameterService;

    public EmployeeService(ParameterService parameterService, EmployeeRepository repository, JobService jobService, CountryService countryService, DepartmentService departmentService, ProjectService projectService, PasswordEncoder passwordEncoder, UserRepository userRepository, EmployeeProjectRepository employeeProjectRepository) {
        this.repository = repository;
        this.jobService = jobService;
        this.countryService = countryService;
        this.departmentService = departmentService;
        this.projectService = projectService;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.employeeProjectRepository = employeeProjectRepository;
        this.parameterService = parameterService;
    }

    public CommonResponse addDataEmployee(EmployeeRequest request) {

        if (repository.existsByEmail(request.getEmail())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Email %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getEmail()),
                    null,
                    HttpStatus.OK);
        }

        String domainWhitelist = parameterService.findByKey("domain_whitelist").getValue();
        String[] domains = domainWhitelist.split(",");
        String emailDomain = request.getEmail().substring(request.getEmail().indexOf('@') + 1);
        boolean isDomainValid = Arrays.stream(domains)
                              .anyMatch(domain -> domain.trim().equalsIgnoreCase(emailDomain));
        if (!isDomainValid) {
            throw new CommonException(
                    CommonMessage.INVALID_REQUEST_CODE,
                    String.format("Email %s is ".concat(CommonMessage.INVALID_MESSAGE).concat(" [Email domain accepted: "+Arrays.toString(domains)+"]"), request.getEmail()),
                    null,
                    HttpStatus.OK);
        }

        if (!Objects.isNull(request.getPhoneNumber()) && repository.existsByPhoneNumber(request.getPhoneNumber())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Phone Number %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getPhoneNumber()),
                    null,
                    null);
        }

        if (Objects.isNull(request.getJobId())) {
            throw new CommonException(
                    CommonMessage.INVALID_REQUEST_CODE,
                    String.format("%s for data job", CommonMessage.INVALID_MESSAGE),
                    null,
                    null
            );
        }

        if (Objects.nonNull(request.getProjectIdList()) && !request.getProjectIdList().isEmpty()) {
            Project project = projectService.getFirstProject();
            request.setProjectIdList(Collections.singleton(project.getProjectId()));
            request.setClientId(project.getClient().getClientId());
        }

        Employee employee = new Employee();
        employee.setFullName(request.getFullName());
        employee.setGender(request.getGender());
        employee.setEmail(request.getEmail());
        employee.setPhoneNumber(request.getPhoneNumber());
        employee.setJoinDate(request.getJoinDate());
        employee.setIsActive(true);
        employee.setCountry(countryService.getCountryById(request.getPlacementCountryId()));
        employee.setJob(jobService.getJobById(request.getJobId()));
        employee.setDepartment(Objects.isNull(request.getDepartmentId()) ? null : departmentService.getDepartmentById(request.getDepartmentId()));
        repository.saveAndFlush(employee);

        if(Objects.nonNull(request.getProjectIdList())){
            request.getProjectIdList().forEach(projectId -> {
                EmployeeProject employeeProject = new EmployeeProject();
                EmployeeProjectKey id = new EmployeeProjectKey();
                id.setEmployeeId(employee.getEmployeeId());
                id.setProjectId(projectId);
                employeeProject.setEmployeeProjectId(id);
                employeeProject.setEmployee(employee);
                employeeProject.setProject(projectService.getProjectById(projectId));
                employeeProject.setStartDate(request.getJoinDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                employeeProject.setEndDate(employeeProject.getStartDate().plusMonths(6));
                employeeProject.setIsActive(true);
                employeeProjectRepository.save(employeeProject);
            });
        }
        
        String password = CommonUtil.tempPassword();

        User user = User.builder()
                .fullName(employee.getFullName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(password))
                .role(CommonUtil.convertRole(request.getRoleName()))
                .build();
        userRepository.save(user);

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE.concat(" Your temporary password: "+password), CommonMessage.SUCCESS_MESSAGE);
    }


    public CommonResponse updateDataEmployee(EmployeeRequest request) {
        Optional<Employee> optionalEmployee  = repository.findByEmail(request.getEmail());
        if(optionalEmployee.isEmpty()){
            throw new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                CommonMessage.NOT_FOUND_MESSAGE,
                null,
                null
             );
        }

        Employee employee = optionalEmployee.get();
        employee.setFullName(request.getFullName());
        employee.setGender(request.getGender());
        employee.setPhoneNumber(request.getPhoneNumber());
        employee.setJoinDate(request.getJoinDate());

        employee.setIsActive(request.isActive());
        employee.setCountry(Objects.isNull(request.getPlacementCountryId())?employee.getCountry():countryService.getCountryById(request.getPlacementCountryId()));
        employee.setJob(Objects.isNull(request.getJobId())?employee.getJob():jobService.getJobById(request.getJobId()));
        employee.setDepartment(Objects.isNull(request.getDepartmentId()) ? employee.getDepartment() : departmentService.getDepartmentById(request.getDepartmentId()));

        repository.save(employee);
        
       return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }


    public CommonResponse listEmployees(int page, int size, String[] sort, String [] search) {
        Page<Employee> employeeList;
        Specification<Employee> specification = isActive();
        if (Objects.nonNull(search) && search.length > 0) {
            String columnName = search[0];
            String searchValue = search[1];

            if (columnName.equalsIgnoreCase("countryName")) {
                specification = hasCountryName(searchValue);
            } else {
                specification = hasName(columnName, searchValue);
            }

            employeeList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else {
            employeeList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        }

        return setValueEmployee(employeeList);
    }

    public CommonResponse getEmployeeByEmployeeId(Long employeeId) {
        Optional<Employee> employee = repository.findById(employeeId);
        if(employee.isEmpty()){
            throw new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                CommonMessage.NOT_FOUND_MESSAGE,
                null,
                null
             );
        }

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setActive(employee.get().getIsActive());
        employeeResponse.setEmployeeId(employee.get().getEmployeeId());
        employeeResponse.setFullName(employee.get().getFullName());
        employeeResponse.setEmail(employee.get().getEmail());
        employeeResponse.setPhoneNumber(employee.get().getPhoneNumber());
        employeeResponse.setJoinDate(employee.get().getJoinDate());
        employeeResponse.setDepartmentName(Objects.isNull(employee.get().getDepartment()) ? null: employee.get().getDepartment().getDepartmentName());
        employeeResponse.setJobTitle(employee.get().getJob().getJobName());
        employeeResponse.setGender(CommonUtil.capitalize(employee.get().getGender().name()));
        employeeResponse.setPlacementCountry(Objects.isNull(employee.get().getCountry()) ? null : employee.get().getCountry().getCountryName());
       
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, employeeResponse);
    }

    public CommonResponse deleteEmployeeById(Long employeeId) {
        Employee employee = repository.findById(employeeId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("Employee ID %d %s", employeeId, CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));

        employee.setIsActive(false);
        repository.save(employee);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse assignJob(AssignJobRequest request) {
        Employee employee = this.getEmployeeById(request.getEmployeeId());
        employee.setJob(jobService.getJobById(request.getJobId()));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse assignDepartment(AssignDepartment request) {
        Employee employee = this.getEmployeeById(request.getEmployeeId());
        employee.setDepartment(departmentService.getDepartmentById(request.getDepartmentId()));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse assignProject(AssignProject request) {
        Employee employee = this.getEmployeeById(request.getEmployeeId());

        Set<EmployeeProject> employeeProjects = new HashSet<>();
        request.getProjectIdList().forEach(projectId -> {
            EmployeeProjectKey key = new EmployeeProjectKey();
            key.setEmployeeId(request.getEmployeeId());
            key.setProjectId(projectId);

            EmployeeProject project = new EmployeeProject();
            project.setEmployeeProjectId(key);
            project.setProject(projectService.getProjectById(projectId));
            project.setEmployee(employee);
            project.setStartDate(LocalDate.now());
            project.setEndDate(LocalDate.now().plusYears(1));
            project.setIsActive(true);
            employeeProjects.add(project);
            employeeProjectRepository.save(project);
        });

        employee.setEmployeeProjects(employeeProjects);
        repository.save(employee);

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public Employee getEmployeeByEmail(String email) {
        Optional<Employee> dataEmployee = repository.findByEmailAndIsActive(email, true);
        return setEmployeeAtomicValue(email, dataEmployee);
    }

    public Employee getEmployeeById(Long employeeId) {
        Optional<Employee> dataEmployee = repository.findByEmployeeIdAndIsActive(employeeId, true);
        return setEmployeeAtomicValue(String.valueOf(employeeId), dataEmployee);
    }

    private CommonResponse setValueEmployee(Page<Employee> employeeList) {
        List<EmployeeResponse> employees = new ArrayList<>();

        employeeList.getContent().forEach(data -> {
            EmployeeResponse employee = new EmployeeResponse();
            employee.setActive(data.getIsActive());
            employee.setEmployeeId(data.getEmployeeId());
            employee.setFullName(data.getFullName());
            employee.setEmail(data.getEmail());
            employee.setPhoneNumber(data.getPhoneNumber());
            employee.setJoinDate(data.getJoinDate());
            employee.setDepartmentName(Objects.isNull(data.getDepartment()) ? null: data.getDepartment().getDepartmentName());
            employee.setJobTitle(data.getJob().getJobName());
            employee.setGender(CommonUtil.capitalize(data.getGender().name()));
            employee.setPlacementCountry(Objects.isNull(data.getCountry()) ? null : data.getCountry().getCountryName());
            employees.add(employee);
        });

        Map<String, Object> response = new HashMap<>();
        response.put("employees", employees);
        response.put("current_page", employeeList.getNumber());
        response.put("total_items", employeeList.getTotalElements());
        response.put("total_pages", employeeList.getTotalPages());
        response.put("page_size", employeeList.getSize());

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
    }

    private static Employee setEmployeeAtomicValue(String employeeId, Optional<Employee> dataEmployee) {
        AtomicReference<Employee> employee = new AtomicReference<>(new Employee());
        dataEmployee.ifPresentOrElse(data -> {
            employee.get().setEmployeeId(data.getEmployeeId());
            employee.get().setFullName(data.getFullName());
            employee.get().setGender(data.getGender());
            employee.get().setEmail(data.getEmail());
            employee.get().setPhoneNumber(data.getPhoneNumber());
            employee.get().setJoinDate(data.getJoinDate());
            employee.get().setIsActive(data.getIsActive());
            employee.get().setJob(data.getJob());
            employee.get().setCountry(data.getCountry());
            employee.get().setDepartment(data.getDepartment());
        }, () -> {
            throw new CommonException(
                    CommonMessage.NOT_FOUND_CODE,
                    String.format("Employee %s %s", employeeId, CommonMessage.NOT_FOUND_MESSAGE),
                    null,
                    HttpStatus.OK);
        });
        return employee.get();
    }
}
