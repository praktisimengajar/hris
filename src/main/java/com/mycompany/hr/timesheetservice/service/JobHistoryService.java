package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.repository.administration.JobHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class JobHistoryService {

    private final JobHistoryRepository repository;

    public JobHistoryService(JobHistoryRepository repository) {
        this.repository = repository;
    }
}
