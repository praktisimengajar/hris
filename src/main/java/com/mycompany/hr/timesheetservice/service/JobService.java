package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.administration.Job;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.administration.JobRequest;
import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.mycompany.hr.timesheetservice.payload.response.administration.JobResponse;
import com.mycompany.hr.timesheetservice.repository.administration.EmployeeRepository;
import com.mycompany.hr.timesheetservice.repository.administration.JobRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.mycompany.hr.timesheetservice.util.specification.JobSpecification.*;

@Service
@Slf4j
public class JobService {
    private final JobRepository repository;
    private final DepartmentService departmentService;

    private final EmployeeRepository employeeRepository;

    public JobService(JobRepository repository, DepartmentService departmentService, EmployeeRepository employeeRepository) {
        this.repository = repository;
        this.departmentService = departmentService;
        this.employeeRepository = employeeRepository;
    }

    public CommonResponse addDataJob(JobRequest request) {
        if (repository.existsByJobName(request.getJobName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Job Name %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getJobName()),
                    null,
                    HttpStatus.OK);
        }
        Job job = new Job();
        job.setJobName(request.getJobName());
        job.setJobDescription(request.getJobDescription());
        job.setDuties(request.getDuties());
        job.setResponsibilities(request.getResponsibilities());
        job.setIsActive(true);
        job.setDepartment(departmentService.getDepartmentById(request.getDepartmenId()));
        repository.save(job);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse getJobByJobId(Long jobId) {
        Job job = this.getJobById(jobId);
        List<Employee> employees = employeeRepository.findAllByIsActiveAndJob(true, job);
        Set<EmployeeResponse> listMembers = new HashSet<>();

        employees.forEach(employee -> {
            EmployeeResponse response = new EmployeeResponse();
            response.setEmployeeId(employee.getEmployeeId());
            response.setFullName(employee.getFullName());
            response.setGender(CommonUtil.capitalize(employee.getGender().name()));
            response.setPlacementCountry(employee.getCountry().getCountryName());
            response.setJobTitle(employee.getJob().getJobName());
            listMembers.add(response);
        });
        JobResponse response = new JobResponse(
                    job.getJobId(), job.getJobName(), job.getJobDescription(), job.getDuties(),
                    job.getResponsibilities(), job.getDepartment().getDepartmentName(),
                    Math.toIntExact(employeeRepository.countByJob_JobId(job.getJobId())),
                    listMembers
                );
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
    }

    public CommonResponse listAllJobs(int page, int size, String[] sort, List<String> search) {
        Page<Job> jobList;

        Specification<Job> specification = isActive();
        if (Objects.isNull(search)) {
            jobList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else if (search.isEmpty()) {
            jobList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else {
            String columnName = search.get(0);
            String searchValue = search.get(1);


            if (search.get(0).equalsIgnoreCase("countryName")) {
                specification = hasDepartmentName(searchValue).and(isActive());
            } else {
                specification = hasName(columnName, searchValue).and(isActive());
            }

            jobList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        }

        List<JobResponse> jobs = new ArrayList<>();
        jobList.forEach(data -> jobs.add(new JobResponse(
                data.getJobId(), data.getJobName(), data.getJobDescription(),
                data.getDuties(), data.getResponsibilities(), data.getDepartment().getDepartmentName(),
                Math.toIntExact(employeeRepository.countByJob_JobId(data.getJobId())), null
        )));

        Map<String, Object> response = new HashMap<>();
        response.put("jobs", jobs);
        response.put("current_page", jobList.getNumber());
        response.put("total_items", jobList.getTotalElements());
        response.put("total_pages", jobList.getTotalPages());
        response.put("page_size", jobList.getSize());

        if (!jobs.isEmpty()) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
        } else {
            return new CommonResponse(CommonMessage.NOT_FOUND_CODE, CommonMessage.NOT_FOUND_MESSAGE, CommonMessage.NOT_FOUND_MESSAGE);
        }
    }

    public Job getJobById(Long jobId) {
        return repository.findById(jobId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for job your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }

    public CommonResponse updateDataJob(JobRequest request) {
        Job job = getJobById(request.getJobId());
        job.setJobName(request.getJobName());
        job.setJobDescription(request.getJobDescription());
        job.setDuties(request.getDuties());
        job.setResponsibilities(request.getResponsibilities());
        job.setIsActive(true);
        repository.save(job);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse deleteJob(Long jobId) {
        Job job = getJobById(jobId);
        job.setIsActive(false);
        repository.save(job);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}
