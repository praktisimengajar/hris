package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.project.Location;
import com.mycompany.hr.timesheetservice.repository.project.LocationRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LocationService {

    private final LocationRepository repository;

    public LocationService(LocationRepository repository) {
        this.repository = repository;
    }

    public Location getLocationByAddress(String streetAddress) {
        return repository.findByStreetAddress(streetAddress).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for location your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }
}
