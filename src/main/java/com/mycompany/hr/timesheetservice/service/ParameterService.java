package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.model.Parameter;
import com.mycompany.hr.timesheetservice.repository.ParameterRepository;
import com.mycompany.hr.timesheetservice.util.enumeration.ParameterType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParameterService {

    private final ParameterRepository parameterRepository;

    @Autowired
    public ParameterService(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    public Parameter findByKey(String key) {
        return parameterRepository.findByKey(key);
    }

    public List<Parameter> findByType(ParameterType type) {
        return parameterRepository.findListByType(type);
    }

    public Parameter create(Parameter parameter) {
        return parameterRepository.save(parameter);
    }

    public Parameter update(Parameter parameter) {
        return parameterRepository.save(parameter);
    }

    public Page<Parameter> getAllParameters(Pageable pageable) {
        return parameterRepository.findAll(pageable);
    }

    public Page<Parameter> getParametersByType(ParameterType type, Pageable pageable) {
        return parameterRepository.findPageByType(type, pageable);
    }
}

