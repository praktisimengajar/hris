package com.mycompany.hr.timesheetservice.service;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.model.project.Client;
import com.mycompany.hr.timesheetservice.model.project.EmployeeProject;
import com.mycompany.hr.timesheetservice.model.project.Project;
import com.mycompany.hr.timesheetservice.payload.CommonResponse;
import com.mycompany.hr.timesheetservice.payload.request.project.AssignManagerProject;
import com.mycompany.hr.timesheetservice.payload.request.project.ProjectRequest;
import com.mycompany.hr.timesheetservice.payload.response.administration.EmployeeResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.ClientResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.LocationResponse;
import com.mycompany.hr.timesheetservice.payload.response.project.ProjectResponse;
import com.mycompany.hr.timesheetservice.repository.administration.EmployeeRepository;
import com.mycompany.hr.timesheetservice.repository.project.ClientRepository;
import com.mycompany.hr.timesheetservice.repository.project.EmployeeProjectRepository;
import com.mycompany.hr.timesheetservice.repository.project.ProjectRepository;
import com.mycompany.hr.timesheetservice.util.CommonMessage;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.mycompany.hr.timesheetservice.util.specification.ProjectSpecification.*;

@Repository
@Slf4j
public class ProjectService {
    private final ProjectRepository repository;
    private final EmployeeRepository employeeRepository;
    private final ClientRepository clientRepository;
    private final EmployeeProjectRepository employeeProjectRepository;

    public ProjectService(ProjectRepository repository, EmployeeRepository employeeRepository, ClientRepository clientRepository, EmployeeProjectRepository employeeProjectRepository) {
        this.repository = repository;
        this.employeeRepository = employeeRepository;
        this.clientRepository = clientRepository;
        this.employeeProjectRepository = employeeProjectRepository;
    }

    public CommonResponse addDataProject(ProjectRequest request) {
        if (repository.existsByProjectName(request.getProjectName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    String.format("Project Name %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getProjectName()),
                    null,
                    HttpStatus.OK);
        }

        if (Objects.isNull(request.getClientId())) {
            throw new CommonException(
                    CommonMessage.NOT_FOUND_CODE,
                    String.format("Client Id %s ".concat(CommonMessage.NOT_FOUND_MESSAGE), request.getClientId()),
                    null,
                    HttpStatus.OK);
        }

        Project project = new Project();
        project.setProjectName(request.getProjectName());
        project.setClient(this.getClientById(request.getClientId()));
        project.setStatus(true);
        project.setIsActive(true);
        repository.save(project);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse listAllProjects(int page, int size, String[] sort, List<String> search) {
        Page<Project> projectList;

        Specification<Project> specification = isActive();

        if (Objects.isNull(search)) {
            projectList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else if (search.isEmpty()) {
            projectList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        } else {
            String columnName = search.get(0);
            String searchValue = search.get(1);

            if (search.get(0).equalsIgnoreCase("clientName")) {
                specification = hasClientName(searchValue).and(isActive());
            } else {
                specification = hasName(columnName, searchValue).and(isActive());
            }

            projectList = repository.findAll(specification, CommonUtil.paging(page, size, sort));
        }

        List<ProjectResponse> projects = projectList.stream().map(data -> new ProjectResponse(
                data.getProjectId(), data.getProjectName(), data.getNotes(),
                Objects.isNull(data.getManager()) ? null : data.getManager().getFullName(),
                new ClientResponse(data.getClient().getClientId(), data.getClient().getClientName(), data.getClient().getEmail(), data.getClient().getNotes(),  data.getClient().getPhoneNumber(),
                    new LocationResponse(data.getClient().getLocation().getStreetAddress(), data.getClient().getLocation().getPostalCode(), data.getClient().getLocation().getCity(), data.getClient().getLocation().getProvince(), data.getClient().getLocation().getCountry().getCountryName()), null),
                Math.toIntExact(employeeProjectRepository.countByProject_ProjectIdAndIsActive(data.getProjectId(), true)), null
        )).toList();

        Map<String, Object> response = new HashMap<>();
        response.put("projects", projects);
        response.put("current_page", projectList.getNumber());
        response.put("total_items", projectList.getTotalElements());
        response.put("total_pages", projectList.getTotalPages());
        response.put("page_size", projectList.getSize());

        if (!projects.isEmpty()) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
        } else {
            return new CommonResponse(CommonMessage.NOT_FOUND_CODE, CommonMessage.NOT_FOUND_MESSAGE, CommonMessage.NOT_FOUND_MESSAGE);
        }
    }

    public CommonResponse getProjectByProjectId(Long projectId) {
        Project project = this.getProjectById(projectId);
        List<EmployeeProject> employees = employeeProjectRepository.findAllByProjectAndProject_IsActive(project, true);
        Set<EmployeeResponse> listMembers = new HashSet<>();

        employees.forEach(list -> {
            Employee employee = employeeRepository.findById(list.getEmployeeProjectId().getEmployeeId()).orElse(new Employee());
            EmployeeResponse response = new EmployeeResponse();
            response.setEmployeeId(employee.getEmployeeId());
            response.setFullName(employee.getFullName());
            response.setGender(CommonUtil.capitalize(employee.getGender().name()));
            response.setPlacementCountry(employee.getCountry().getCountryName());
            response.setJobTitle(employee.getJob().getJobName());
            listMembers.add(response);
        });
        ProjectResponse response = new ProjectResponse(
                project.getProjectId(), project.getProjectName(), project.getNotes(),
                Objects.isNull(project.getManager()) ? null : project.getManager().getFullName(),
                new ClientResponse(project.getClient().getClientId(), project.getClient().getClientName(), project.getClient().getEmail(), project.getClient().getNotes(),  project.getClient().getPhoneNumber(),
                new LocationResponse(project.getClient().getLocation().getStreetAddress(), project.getClient().getLocation().getPostalCode(), project.getClient().getLocation().getCity(), project.getClient().getLocation().getProvince(), project.getClient().getLocation().getCountry().getCountryName()), null),
                null, listMembers
        );
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, response);
    }

    public Project getProjectById(Long projectId) {
        return repository.findById(projectId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for project your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }

    public CommonResponse assignManager(AssignManagerProject request) {
        Optional<Project> dataProject = repository.findById(request.getProjectId());
        AtomicReference<String> info = new AtomicReference<>();
        dataProject.ifPresentOrElse(data -> employeeRepository.findById(request.getManagerId()).ifPresentOrElse(manager -> {
            data.setManager(manager);
            repository.save(data);
            info.set(CommonMessage.SUCCESS_MESSAGE);
        }, ()-> info.set(CommonMessage.NOT_FOUND_MESSAGE)), () -> info.set(CommonMessage.NOT_FOUND_MESSAGE));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, info.get(), info.get());
    }


    private Client getClientById(Long clientId) {
        return clientRepository.findById(clientId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for client your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }

    public Project getFirstProject() {
        return repository.findTopByStatusOrderByProjectIdDesc(true).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                String.format("%s for job your choose", CommonMessage.NOT_FOUND_MESSAGE),
                null,
                HttpStatus.OK));
    }

    public CommonResponse updateDataProject(ProjectRequest request) {
        Project project = getProjectById(request.getProjectId());
        project.setProjectName(request.getProjectName());
        project.setClient(this.getClientById(request.getClientId()));
        project.setStatus(true);
        repository.save(project);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse deleteProject(Long projectId) {
        Project project = getProjectById(projectId);
        project.setStatus(false);
        repository.save(project);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
}
