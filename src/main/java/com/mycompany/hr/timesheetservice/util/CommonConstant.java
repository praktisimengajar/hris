package com.mycompany.hr.timesheetservice.util;

import java.time.LocalTime;

public class CommonConstant {

    private CommonConstant() {}

    public static final String REGISTER_QUEUE = "register_queue";
    public static final String REGISTER_EXCHANGE = "register_exchange";
    public static final String REGISTER_ROUTING_KEY = "register_routing_key";

    public static final String CLOCK_IN_QUEUE = "clock_in_queue";
    public static final String CLOCK_IN_EXCHANGE = "clock_in_exchange";
    public static final String CLOCK_IN_ROUTING_KEY = "clock_in_routing_key";

    public static final String CLOCK_OUT_QUEUE = "clock_out_queue";
    public static final String CLOCK_OUT_EXCHANGE = "clock_out_exchange";
    public static final String CLOCK_OUT_ROUTING_KEY = "clock_out_routing_key";

    public static final LocalTime WORK_START_TIME = LocalTime.of(9, 0);
}
