package com.mycompany.hr.timesheetservice.util;

public class CommonMessage {

    private CommonMessage() {}

    public static final String SUCCESS_CODE = "200000";
    public static final String SUCCESS_MESSAGE = "success";

    public static final String ERROR_CODE = "200500";
    public static final String ERROR_MESSAGE = "Transaction failed";

    public static final String INVALID_REQUEST_CODE = "200400";
    public static final String INVALID_REQUEST_MESSAGE = "Invalid request.";

    public static final String NOT_FOUND_CODE = "200404";
    public static final String NOT_FOUND_MESSAGE = "data not found";

    public static final String ALREADY_EXIST_CODE = "200409";
    public static final String ALREADY_EXIST_MESSAGE = "already exist";

    public static final String INTEGRITY_VIOLATION_CODE = "200422";
    public static final String INTEGRITY_VIOLATION_MESSAGE = "Data integrity violation.";

    public static final String INVALID_AUTH_CODE = "200401";
    public static final String INVALID_AUTH_MESSAGE = "invalid authorization";

    public static final String INVALID_AUTHENTICATION_CODE = "200403";
    public static final String INVALID_AUTHENTICATION_MESSAGE = "Invalid username or password.";


    public static final String INVALID_MESSAGE = "invalid";
    public static final String ALREADY_CLOCK_IN = "User has already clocked in for today.";
    public static final String ALREADY_CLOCK_OUT = "User has already clocked out for today.";
    public static final String NOT_YET_CLOCK_IN = "User has no clocked in yet for today.";

    
}
