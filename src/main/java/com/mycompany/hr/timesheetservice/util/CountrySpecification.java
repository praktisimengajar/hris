package com.mycompany.hr.timesheetservice.util;

import com.mycompany.hr.timesheetservice.model.administration.Country;
import com.mycompany.hr.timesheetservice.model.project.Region;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;


public class CountrySpecification {
    public static Specification<Country> hasRegionName(String regionName) {
        return (root, query, criteriaBuilder) -> {
            Join<Country, Region> countryRegionJoin = root.join("region");
            return criteriaBuilder.like(
                criteriaBuilder.lower(countryRegionJoin.get("regionName")),
                "%" + regionName.toLowerCase() + "%"
            );
        };
    }
}
