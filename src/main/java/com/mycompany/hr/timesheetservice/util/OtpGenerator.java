package com.mycompany.hr.timesheetservice.util;

import com.mycompany.hr.timesheetservice.exception.CommonException;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class OtpGenerator {
    private Cache<String, OTPData> cache;
    private long expirationTime; // 5 minutes in milliseconds
    private Random random;
    private int maxAttempts;
    private int waitTime;

    /**
     * Constructor configuration.
     */
    public OtpGenerator() {
        expirationTime = 5L * 60 * 1000; // 5 minutes in milliseconds
        random = new Random();
        maxAttempts = 3;
        waitTime = 10 * 60 * 1000; // 10 minutes in milliseconds
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(expirationTime, TimeUnit.MILLISECONDS)
                .build();
        
    }

    /**
     * Method for generating OTP and put it in cache.
     *
     * @param key - cache key
     * @return cache value (generated OTP number)
     * @throws Exception
     */
    public String generateOTP(String email) throws Exception {
        long currentTime = System.currentTimeMillis();

        // Check if the number of attempts has exceeded the limit
        int attempts = getAttempts(email);
        log.info("attempts :"+ attempts);
        if (attempts >= maxAttempts) {
            OTPData otpData = cache.getIfPresent(email);
            if (otpData != null) {
                long waitPeriod = otpData.getTimestamp() + waitTime - currentTime;
                if (waitPeriod > 0) {
                    throw new IllegalStateException("Maximum attempts exceeded (3x). Please wait for " + (waitPeriod / 1000 / 60) + " minutes.");
                }
                resetAttempts(email);
            }
        }

        incrementAttempts(email);

         // Generate RSA KeyPair
        KeyPair keyPair = this.generateRSAKeyPair();

        // Check if there is a valid OTP in the cache for the email
        OTPData otpData = cache.getIfPresent(email);
        if (otpData != null && currentTime - otpData.getTimestamp() <= expirationTime) {
            return decryptWithRSA(keyPair.getPrivate(),otpData.getEncryptedOtp());
        }
         // If no valid OTP found in the cache, generate a new one
        String newOTP = generateNewOTP();
       
        otpData = new OTPData(encryptWithRSA(keyPair.getPublic(),newOTP), currentTime, attempts + 1, false);
        cache.put(email, otpData);
       
         return newOTP;

    }

    private int getAttempts(String email) {
        OTPData otpData = cache.getIfPresent(email);
        return (otpData != null) ? otpData.getAttempts() : 0;
    }

    private void incrementAttempts(String email) {
        OTPData otpData = cache.getIfPresent(email);
        if (otpData != null) {
            otpData = new OTPData(otpData.getEncryptedOtp(), otpData.getTimestamp(), otpData.getAttempts() + 1, false);
            cache.put(email, otpData);
        }
    }
    private void resetAttempts(String email) {
        cache.invalidate(email);
    }
    private String generateNewOTP() {
        // Generate a 4-digit OTP
        int otp = random.nextInt(9000) + 1000;
        return String.valueOf(otp);
    }

    public boolean isVerified(String key){
        OTPData otpData = cache.getIfPresent(key);
        if (otpData != null) {
            return otpData.isVerified() ;
        }
        return false;
    }

    public void verifyOTP(String key, String otp) throws Exception{
        OTPData otpData = cache.getIfPresent(key);
       
        
        if (otpData != null && otp != null ) {
             // Generate RSA KeyPair
             KeyPair keyPair = this.generateRSAKeyPair();
            if(!otp.equals(decryptWithRSA(keyPair.getPrivate(),otpData.getEncryptedOtp())))
            {
                throw new CommonException(
                    CommonMessage.NOT_FOUND_CODE,
                    String.format("OTP Code  %s is ".concat(CommonMessage.INVALID_MESSAGE), otp),
                    null,
                    HttpStatus.OK
                );
            }
            otpData.setVerified(true);
            cache.put(key, otpData);
        }
    }

    private KeyPair generateRSAKeyPair() throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);  // or higher, for stronger security
        return keyGen.generateKeyPair();
    }

    private String encryptWithRSA(PublicKey publicKey, String otp) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedBytes = cipher.doFinal(otp.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    private String decryptWithRSA(PrivateKey privateKey, String encryptedOtp) throws Exception {
        byte[] encryptedBytes = Base64.getDecoder().decode(encryptedOtp);
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        return new String(decryptedBytes, StandardCharsets.UTF_8);
    }


    @Setter
    @Getter
    private static class OTPData {
        private String encryptedOtp;
        private long timestamp;
        private int attempts;
        private boolean isVerified;

        public OTPData(String encryptedOtp, long timestamp, int attempts,boolean isVerified) {
            this.encryptedOtp = encryptedOtp;
            this.timestamp = timestamp;
            this.attempts = attempts;
            this.isVerified = isVerified;
        }
    }
}
