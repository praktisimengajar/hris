package com.mycompany.hr.timesheetservice.util;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    public boolean isValid(String password, ConstraintValidatorContext cxt) {
        return CommonValidator.passwordIsValid(password);
    }
}
