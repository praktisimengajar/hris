package com.mycompany.hr.timesheetservice.util.enumeration;

public enum AttendanceStatus {
    ONTIME, LATE, LEAVE, NORECORD
}
