package com.mycompany.hr.timesheetservice.util.enumeration;

public enum EventType {
    ANNUAL_LEAVE, SICK_LEAVE, MATERNITY_LEAVE, OTHER, PUBLIC_HOLIDAY, ATTEND
}
