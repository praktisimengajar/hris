package com.mycompany.hr.timesheetservice.util.enumeration;

public enum Gender {
    MALE, FEMALE
}
