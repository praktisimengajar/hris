package com.mycompany.hr.timesheetservice.util.enumeration;

public enum ParameterType {
    SYSTEM,
    BUSINESS
}
