package com.mycompany.hr.timesheetservice.util.enumeration;

public enum PostStatus {
    DRAFT, PUBLISHED
}
