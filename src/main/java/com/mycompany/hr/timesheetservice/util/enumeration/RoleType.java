package com.mycompany.hr.timesheetservice.util.enumeration;

public enum RoleType {
    ROLE_USER, ROLE_ADMIN, ROLE_HR, ROLE_BOD, ROLE_FINANCE, ROLE_GUESS
}
