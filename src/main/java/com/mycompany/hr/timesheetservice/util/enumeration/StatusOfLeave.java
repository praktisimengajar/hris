package com.mycompany.hr.timesheetservice.util.enumeration;

public enum StatusOfLeave {
    APPROVED,DECLINED,WAITING
}
