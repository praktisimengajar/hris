package com.mycompany.hr.timesheetservice.util.enumeration;

public enum TimesheetClientCode {
    ICM, ITG, MBB, OTHER
}
