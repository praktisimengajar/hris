package com.mycompany.hr.timesheetservice.util.enumeration;

public enum Workplace {
    WFO, WFH
}
