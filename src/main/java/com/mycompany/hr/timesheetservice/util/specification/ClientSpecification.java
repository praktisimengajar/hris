package com.mycompany.hr.timesheetservice.util.specification;

import com.mycompany.hr.timesheetservice.model.administration.Country;
import com.mycompany.hr.timesheetservice.model.project.Client;
import com.mycompany.hr.timesheetservice.model.project.Location;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

public class ClientSpecification {

    private ClientSpecification() {}

    public static Specification<Client> hasName(String column, String value) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(column), "%" + CommonUtil.capitalize(value) + "%");
    }

    public static Specification<Client> isActive() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isActive"), true);
    }

    public static Specification<Client> hasCountryName(String countryName) {
        return (root, query, criteriaBuilder) -> {
            Join<Client, Location> clientLocation = root.join("location");
            Join<Location, Country> clientCountry = clientLocation.join("country");
            return criteriaBuilder.equal(clientCountry.get("countryName"), CommonUtil.capitalize(countryName));
        };
    }
}
