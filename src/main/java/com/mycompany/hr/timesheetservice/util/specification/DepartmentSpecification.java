package com.mycompany.hr.timesheetservice.util.specification;

import com.mycompany.hr.timesheetservice.model.administration.Department;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import org.springframework.data.jpa.domain.Specification;

public class DepartmentSpecification {

    private DepartmentSpecification() {}

    public static Specification<Department> hasName(String column, String value) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(column), "%" + CommonUtil.capitalize(value) + "%");
    }

    public static Specification<Department> isActive() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isActive"), true);
    }
}
