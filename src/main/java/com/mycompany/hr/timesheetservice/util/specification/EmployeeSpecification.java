package com.mycompany.hr.timesheetservice.util.specification;

import com.mycompany.hr.timesheetservice.model.administration.Country;
import com.mycompany.hr.timesheetservice.model.administration.Employee;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

public class EmployeeSpecification {

    private EmployeeSpecification() {}

    public static Specification<Employee> isActive() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isActive"), true);
    }

    public static Specification<Employee> hasName(String column, String value) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(column), "%" + CommonUtil.capitalize(value) + "%");
    }

    public static Specification<Employee> hasCountryName(String countryName) {
        return (root, query, criteriaBuilder) -> {
            Join<Employee, Country> employeeCountry = root.join("country");
            return criteriaBuilder.equal(employeeCountry.get("countryName"), CommonUtil.capitalize(countryName));
        };
    }
}
