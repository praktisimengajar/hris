package com.mycompany.hr.timesheetservice.util.specification;

import com.mycompany.hr.timesheetservice.model.administration.Department;
import com.mycompany.hr.timesheetservice.model.administration.Job;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

public class JobSpecification {

    private JobSpecification() {}

    public static Specification<Job> hasName(String column, String value) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(column), "%" + CommonUtil.capitalize(value) + "%");
    }

    public static Specification<Job> isActive() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isActive"), true);
    }

    public static Specification<Job> hasDepartmentName(String departmentName) {
        return (root, query, criteriaBuilder) -> {
            Join<Job, Department> jobDepartment = root.join("department");
            return criteriaBuilder.equal(jobDepartment.get("departmentName"), CommonUtil.capitalize(departmentName));
        };
    }
}
