package com.mycompany.hr.timesheetservice.util.specification;

import com.mycompany.hr.timesheetservice.model.project.Client;
import com.mycompany.hr.timesheetservice.model.project.Project;
import com.mycompany.hr.timesheetservice.util.CommonUtil;
import jakarta.persistence.criteria.Join;
import org.springframework.data.jpa.domain.Specification;

public class ProjectSpecification {

    private ProjectSpecification() {}

    public static Specification<Project> hasName(String column, String value) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(column), "%" + CommonUtil.capitalize(value) + "%");
    }

    public static Specification<Project> isActive() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isActive"), true);
    }

    public static Specification<Project> hasClientName(String clientName) {
        return (root, query, criteriaBuilder) -> {
            Join<Project, Client> jobDepartment = root.join("client");
            return criteriaBuilder.equal(jobDepartment.get("clientName"), CommonUtil.capitalize(clientName));
        };
    }
}
