INSERT INTO leave_type (leave_type_id, name, created_by, created_date)
VALUES
    (1, 'Family Emergency', 'system', CURRENT_DATE),
    (2, 'Maternity Leave', 'system', CURRENT_DATE),
    (3, 'Personal Leave', 'system', CURRENT_DATE),
    (4, 'Religious observances', 'system', CURRENT_DATE),
    (5, 'Sick Leave', 'system', CURRENT_DATE);


INSERT INTO clients (client_id, client_name, address, email, notes, created_date, created_by)
VALUES (1, 'PT. MyCompany Consulting Indonesia', 'Tower II 2nd Floor, Scientia Business Park, Tangerang Regency, Banten 15810', 'digitaladmin@mycompany.id', 'No Notes', now(), 'system');
INSERT INTO projects (project_id, project_name, client_id, status, created_date, created_by) VALUES (1, 'internal project', 1, true, now(), 'system');


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO parameters (parameter_id, key, value, type) VALUES 
(uuid_generate_v4(), 'domain_whitelist', 'mycompany.id,mycompany.com', 'SYSTEM');

-- INSERT INTO parameters (parameter_id, key, value, type) VALUES 
-- (uuid_generate_v4(), 'max_file_upload_size', '5000000', 'SYSTEM'),
-- (uuid_generate_v4(), 'log_level', 'INFO', 'SYSTEM'),
-- (uuid_generate_v4(), 'database_timeout', '30', 'SYSTEM'),
-- (uuid_generate_v4(), 'maintenance_mode', 'false', 'SYSTEM'),
-- (uuid_generate_v4(), 'sales_tax_rate', '0.07', 'BUSINESS'),
-- (uuid_generate_v4(), 'default_user_role', 'USER', 'BUSINESS'),
-- (uuid_generate_v4(), 'max_login_attempts', '5', 'BUSINESS'),
-- (uuid_generate_v4(), 'password_complexity_rule', 'At least 8 characters, including 1 digit and 1 uppercase letter', 'BUSINESS');
